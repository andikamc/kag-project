<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["prefix"=>"admin","middleware"=>"auth"], function(){

	Route::get("dashboard", "AdminController@index")->name("admin.dashboard");

	// content
	Route::group(["prefix"=>"content"], function(){

		// tags
		Route::get("tags", "AdminController@tagContent")->name("admin.contents.tags");
		Route::post("tags/store", "AdminController@tagContentStore")->name("admin.contents.tags.store");
		Route::post("tags/update/{tags_id}", "AdminController@tagContentUpdate")->name("admin.contents.tags.update");
		Route::get("tags/remove/{tags_id}", "AdminController@tagContentRemove")->name("admin.contents.tags.remove");

		// portofolio
		Route::get("portofolios", "AdminController@portofolioContent")->name("admin.contents.portofolios");
		Route::get("portofolios/create", "AdminController@portofolioContentCreate")->name("admin.contents.portofolios.create");
		Route::post("portofolios/store", "AdminController@portofolioContentStore")->name("admin.contents.portofolios.store");
		Route::get("portofolios/edit/{portofolios_id}", "AdminController@portofolioContentEdit")->name("admin.contents.portofolios.edit");
		Route::post("portofolios/update/{portofolios_id}", "AdminController@portofolioContentUpdate")->name("admin.contents.portofolios.update");
		Route::get("portofolios/remove/{portofolios_id}", "AdminController@portofolioContentRemove")->name("admin.contents.portofolios.remove");

		// media
		Route::get("medias", "AdminController@mediaContent")->name("admin.contents.medias");
		Route::get("medias/create", "AdminController@mediaContentCreate")->name("admin.contents.medias.create");
		Route::post("medias/store", "AdminController@mediaContentStore")->name("admin.contents.medias.store");
		Route::get("medias/remove/{medias_id}", "AdminController@mediaContentRemove")->name("admin.contents.medias.remove");

		// message
		Route::get("messages", "AdminController@messageContent")->name("admin.contents.messages");

		// blog
		Route::group(["prefix"=>"blogs"], function(){

			// blog
			Route::get("/", "AdminController@blogContent")->name("admin.contents.blogs");
			Route::get("/create", "AdminController@blogContentCreate")->name("admin.contents.blogs.create");
			Route::post("/store", "AdminController@blogContentStore")->name("admin.contents.blogs.store");
			Route::get("/edit/{blog_id}", "AdminController@blogContentEdit")->name("admin.contents.blogs.edit");
			Route::post("/update/{blog_id}", "AdminController@blogContentUpdate")->name("admin.contents.blogs.update");
			Route::get("/remove/{blog_id}", "AdminController@blogContentRemove")->name("admin.contents.blogs.remove");

			// category
			Route::get("categories", "AdminController@blogCategoriesContent")->name("admin.contents.blogs.categories");
			Route::post("categories/store", "AdminController@blogCategoriesContentStore")->name("admin.contents.blogs.categories.store");
			Route::post("categories/update/{categories_id}", "AdminController@blogCategoriesContentUpdate")->name("admin.contents.blogs.categories.update");
			Route::get("categories/remove/{categories_id}", "AdminController@blogCategoriesContentRemove")->name("admin.contents.blogs.categories.remove");

			// comment
			Route::get("comments", "AdminController@blogCommentsContent")->name("admin.contents.blogs.comments");

		});

		// service
		Route::group(["prefix"=>"services"], function(){
			Route::get("", "AdminController@serviceContent")->name("admin.contents.services");
			Route::get("/create", "AdminController@serviceContentCreate")->name("admin.contents.services.create");
			Route::post("/store", "AdminController@serviceContentStore")->name("admin.contents.services.store");
			Route::get("/edit/{services_id}", "AdminController@serviceContentEdit")->name("admin.contents.services.edit");
			Route::post("/update/{services_id}", "AdminController@serviceContentUpdate")->name("admin.contents.services.update");
			Route::get("/remove/{services_id}", "AdminController@serviceContentRemove")->name("admin.contents.services.remove");
		});

		// slider
		Route::group(["prefix"=>"sliders"], function(){
			Route::get("", "AdminController@sliderContent")->name("admin.contents.sliders");
			Route::get("/create", "AdminController@sliderContentCreate")->name("admin.contents.sliders.create");
			Route::post("/store", "AdminController@sliderContentStore")->name("admin.contents.sliders.store");
			Route::get("/edit/{sliders_id}", "AdminController@sliderContentEdit")->name("admin.contents.sliders.edit");
			Route::post("/update/{sliders_id}", "AdminController@sliderContentUpdate")->name("admin.contents.sliders.update");
			Route::get("/remove/{sliders_id}", "AdminController@sliderContentRemove")->name("admin.contents.sliders.remove");
		});

		// client
		Route::group(["prefix"=>"clients"], function(){
			Route::get("", "AdminController@clientContent")->name("admin.contents.clients");
			Route::get("/create", "AdminController@clientContentCreate")->name("admin.contents.clients.create");
			Route::post("/store", "AdminController@clientContentStore")->name("admin.contents.clients.store");
			Route::get("/edit/{clients_id}", "AdminController@clientContentEdit")->name("admin.contents.clients.edit");
			Route::post("/update/{clients_id}", "AdminController@clientContentUpdate")->name("admin.contents.clients.update");
			Route::get("/remove/{clients_id}", "AdminController@clientContentRemove")->name("admin.contents.clients.remove");
		});

		// team
		Route::group(["prefix"=>"teams"], function(){
			Route::get("", "AdminController@teamContent")->name("admin.contents.teams");
			Route::get("/create", "AdminController@teamContentCreate")->name("admin.contents.teams.create");
			Route::post("/store", "AdminController@teamContentStore")->name("admin.contents.teams.store");
			Route::get("/edit/{teams_id}", "AdminController@teamContentEdit")->name("admin.contents.teams.edit");
			Route::post("/update/{teams_id}", "AdminController@teamContentUpdate")->name("admin.contents.teams.update");
			Route::get("/remove/{teams_id}", "AdminController@teamContentRemove")->name("admin.contents.teams.remove");
		});

		// testimonial
		Route::group(["prefix"=>"testimonials"], function(){
			Route::get("", "AdminController@testimonialContent")->name("admin.contents.testimonials");
			Route::get("/create", "AdminController@testimonialContentCreate")->name("admin.contents.testimonials.create");
			Route::post("/store", "AdminController@testimonialContentStore")->name("admin.contents.testimonials.store");
			Route::get("/edit/{testimonials_id}", "AdminController@testimonialContentEdit")->name("admin.contents.testimonials.edit");
			Route::post("/update/{testimonials_id}", "AdminController@testimonialContentUpdate")->name("admin.contents.testimonials.update");
			Route::get("/remove/{testimonials_id}", "AdminController@testimonialContentRemove")->name("admin.contents.testimonials.remove");
		});

	});

	// setting
	Route::group(["prefix"=>"setting"], function(){

		// general
		Route::get("general", "AdminController@settingGeneral")->name("admin.setting.general");
		Route::get("company", "AdminController@settingCompany")->name("admin.setting.company");
		Route::get("contact", "AdminController@settingContact")->name("admin.setting.contact");
		Route::post("general/store", "AdminController@settingGeneralStore")->name("admin.setting.general.store");

	});

});