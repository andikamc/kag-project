<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Front\HomeController@index');

// services
Route::get("service/{id}", "Front\PublicController@serviceContent")->name("front.service.content");
// Portfolio
Route::get("portfolio", "Front\PublicController@portfolio")->name("front.portfolio");
// Blog
Route::get("blog", "Front\PublicController@blog")->name("front.blog");
Route::get("blog/category/{category_slug}", "Front\PublicController@blogCategory")->name("front.blog.category");
Route::get("blog/tags/{tag_slug}", "Front\PublicController@blogTag")->name("front.blog.tag");
Route::get("blog/{slug_post}", "Front\PublicController@blogDetail")->name("front.blog.detail");
Route::post("blog/{slug_post}/comment/store", "Front\PublicController@blogCommentStore")->name("front.blog.comment.store");
// Contact
Route::get("contact", "Front\PublicController@contact")->name("front.contact");
Route::post("contact/store", "Front\PublicController@contactStore")->name("front.contact.store");
// About
Route::group(["prefix"=>"about"], function(){
	Route::get("description", "Front\PublicController@aboutDescription")->name("front.about-description");
	Route::get("visimisi", "Front\PublicController@aboutVisiMisi")->name("front.about-visimisi");
	Route::get("history", "Front\PublicController@aboutHistory")->name("front.about-history");
	Route::get("team", "Front\PublicController@aboutTeam")->name("front.about-team");
	Route::get("client", "Front\PublicController@aboutClient")->name("front.about-client");
	Route::get("testimonial", "Front\PublicController@aboutTestimonial")->name("front.about-testimonial");
});

Auth::routes();

Route::get('home', function(){
	return redirect(url('admin/dashboard'));
});

require __DIR__."/admin.php";