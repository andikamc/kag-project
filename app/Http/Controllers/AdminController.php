<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use Auth;
use App\Models\Setting;
use App\Models\Message;
use App\Models\Media;
use App\Models\Tag;
use App\Models\Service;
use App\Models\Service_image;
use App\Models\Slider;
use App\Models\Client;
use App\Models\Team;
use App\Models\Testimonial;
use App\Models\Portofolio_post;
use App\Models\Portofolio_tag;
use App\Models\Blog_tag;
use App\Models\Blog_post;
use App\Models\Blog_comment;
use App\Models\Blog_gallery;
use App\Models\Blog_category;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function get_slug($string)
    {
        return str_replace('_', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', $string));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * ================================================
     */

    public function tagContent()
    {
        return view("admin.contents.tag.list")->with([
            "tags" => Tag::get()
        ]);
    }

    public function tagContentStore(Request $req)
    {
        try {
            if (Tag::where("slug", $req->slug)->count() == 0)
            {
                DB::transaction(function() use ($req){
                    Tag::create([
                        "name"  =>  strtolower($req->name),
                        "slug"  =>  $this->get_slug(strtolower($req->name))
                    ]);
                });
                return redirect(route("admin.contents.tags"));
            }
            return redirect(route("admin.contents.tags"));
        } catch (Exception $e) {
            return;
        }
    }

    public function tagContentRemove($tags_id)
    {
        try {
            if (Tag::where("id", $tags_id)->count() > 0)
            {
                Tag::where("id", $tags_id)->delete();
                return redirect(route("admin.contents.tags"));
            }
            return redirect(route("admin.contents.tags"));
        } catch (Exception $e) {
            return;
        }
    }

    public function tagContentUpdate(Request $req, $tags_id)
    {
        try {
            if (Tag::where("id", $tags_id)->count() > 0)
            {
                Tag::where("id", $tags_id)->update([
                    "name"  =>  strtolower($req->name),
                    "slug"  =>  $this->get_slug(strtolower($req->name))
                ]);
                return redirect(route("admin.contents.tags"));
            }
            return redirect(route("admin.contents.tags"));
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * ================================================
     */

    public function portofolioContent()
    {
        return view("admin.contents.portofolio.list")->with([
            "portofolios" => Portofolio_post::get()
        ]);
    }

    public function portofolioContentCreate()
    {
        return view("admin.contents.portofolio.create")->with([
            "tags"   => Tag::get(),
            "medias" => Media::get()
        ]);
    }

    public function portofolioContentEdit($portofolio_post_id)
    {
        return view("admin.contents.portofolio.update")->with([
            "portofolio" => Portofolio_post::where("id", $portofolio_post_id)->first(),
            "tags"   => Tag::get(),
            "medias" => Media::get()
        ]);
    }

    public function portofolioContentStore(Request $req)
    {
        // return $req;
        try {
            DB::transaction(function() use ($req){
                $portofolio_post = Portofolio_post::create([
                    "title"      =>  $req->title,
                    "content"    =>  $req->content,
                    "medias_id"  =>  $req->medias_id,
                ]);

                if ($req->tags_id)
                {
                    foreach ($req->tags_id as $portofolio_tag)
                    {
                        Portofolio_tag::create([
                            "portofolio_posts_id"   => $portofolio_post->id,
                            "tags_id"               => $portofolio_tag
                        ]);
                    }
                }
            });
            return redirect(route("admin.contents.portofolios"));
        } catch (Exception $e) {
            return;
        }
    }

    public function portofolioContentRemove($portofolio_post_id)
    {
        try {
            if (Portofolio_post::where("id", $portofolio_post_id)->count() > 0)
            {
                Portofolio_tag::where("portofolio_posts_id", $portofolio_post_id)->delete();
                Portofolio_post::where("id", $portofolio_post_id)->delete();

                return redirect(route("admin.contents.portofolios"));
            }
            return redirect(route("admin.contents.portofolios"));
        } catch (Exception $e) {
            return;
        }
    }

    public function portofolioContentUpdate(Request $req, $portofolio_post_id)
    {
        try {
            DB::transaction(function() use ($req, $portofolio_post_id){
                if (Portofolio_post::where("id", $portofolio_post_id)->count() > 0)
                {
                    Portofolio_post::where("id", $portofolio_post_id)->update([
                        "title"      =>  $req->title,
                        "content"    =>  $req->content,
                        "medias_id"  =>  $req->medias_id,
                    ]);

                    Portofolio_tag::where("portofolio_posts_id", $portofolio_post_id)->delete();

                    if ($req->tags_id)
                    {
                        foreach ($req->tags_id as $portofolio_tag)
                        {
                            Portofolio_tag::create([
                                "portofolio_posts_id"   => $portofolio_post_id,
                                "tags_id"               => $portofolio_tag
                            ]);
                        }
                    }
                }
            });
            return redirect(route("admin.contents.portofolios"));
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * ================================================
     */

    public function serviceContent()
    {
        return view("admin.contents.service.list")->with([
            "services" => Service::get()
        ]);
    }

    public function serviceContentCreate()
    {
        return view("admin.contents.service.create")->with([
            "medias" => Media::get()
        ]);
    }

    public function serviceContentStore(Request $req)
    {
        try {
            DB::transaction(function() use ($req){
                $service = Service::create([
                    "slug"      =>  $this->get_slug($req->title),
                    "title"     =>  $req->title,
                    "content"   =>  $req->content,
                    "icon"      =>  $req->icon,
                ]);

                if ($req->medias_id)
                {
                    foreach ($req->medias_id as $media)
                    {
                        Service_image::create([
                            "services_id"   => $service->id,
                            "medias_id"     => $media
                        ]);
                    }
                }
            });
            return redirect(route("admin.contents.services"));
        } catch (Exception $e) {
            return;
        }
    }

    public function serviceContentRemove($services_id)
    {
        try {
            DB::transaction(function() use ($services_id){
                if (Service_image::where("services_id", $services_id)->count() > 0)
                {
                    Service_image::where("services_id", $services_id)->forceDelete();
                }
                if (Service::where("id", $services_id)->count() > 0)
                {
                    Service::where("id", $services_id)->forceDelete();
                }
            });
            return redirect(route("admin.contents.services"));
        } catch (Exception $e) {
            return;
        }
    }

    public function serviceContentEdit($services_id)
    {
        $images = Service_image::select('medias_id')->where("services_id", $services_id)->get()->implode('medias_id', ',');
        return view("admin.contents.service.create")->with([
            "service"       => Service::where("id", $services_id)->first(),
            "service_image" => $images,
            "medias"        => Media::get()
        ]);
    }

    public function serviceContentUpdate(Request $req, $services_id)
    {
        // return $services_id;
        try {
            if (Service::where("id", $services_id)->count() > 0)
            {
                Service::where("id", $services_id)->update([
                    "title"     =>  $req->title,
                    "content"   =>  $req->content,
                    "icon"      =>  $req->icon,
                ]);

                if ($req->medias_id)
                {
                    if (Service_image::where("services_id", $services_id)->count() > 0)
                    {
                        Service_image::where("services_id", $services_id)->forceDelete();
                    }

                    foreach ($req->medias_id as $media)
                    {
                        Service_image::create([
                            "services_id"   => $services_id,
                            "medias_id"     => $media
                        ]);
                    }
                }
            }
            return redirect(route("admin.contents.services"));
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * ================================================
     */    

    public function sliderContent()
    {
        return view("admin.contents.slider.list")->with([
            "sliders" => Slider::get()
        ]);
    }

    public function sliderContentCreate()
    {
        return view("admin.contents.slider.create")->with([
            "medias" => Media::get()
        ]);
    }

    public function sliderContentStore(Request $req)
    {
        // return $req;
        try {
            DB::transaction(function() use ($req){
                $slider = Slider::create([
                    "title"         =>  $req->title,
                    "description"   =>  $req->description,
                    "medias_id"     =>  $req->medias_id,
                ]);
            });
            return redirect(route("admin.contents.sliders"));
        } catch (Exception $e) {
            return;
        }
    }

    public function sliderContentRemove($sliders_id)
    {
        try {
            DB::transaction(function() use ($sliders_id){
                if (Slider::where("id", $sliders_id)->count() > 0)
                {
                    Slider::where("id", $sliders_id)->forceDelete();
                }
            });
            return redirect(route("admin.contents.sliders"));
        } catch (Exception $e) {
            return;
        }
    }

    public function sliderContentEdit($sliders_id)
    {
        return view("admin.contents.slider.create")->with([
            "slider"       => Slider::where("id", $sliders_id)->first(),
            "medias"        => Media::get()
        ]);
    }

    public function sliderContentUpdate(Request $req, $sliders_id)
    {
        try {
            if (Slider::where("id", $sliders_id)->count() > 0)
            {
                Slider::where("id", $sliders_id)->update([
                    "title"     =>  $req->title,
                    "description"   =>  $req->description,
                    "medias_id"     =>  $req->medias_id,
                ]);
            }
            return redirect(route("admin.contents.sliders"));
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * ================================================
     */

    public function clientContent()
    {
        return view("admin.contents.client.list")->with([
            "clients" => Client::get()
        ]);
    }

    public function clientContentCreate()
    {
        return view("admin.contents.client.create")->with([
            "medias" => Media::get()
        ]);
    }

    public function clientContentStore(Request $req)
    {
        try {
            DB::transaction(function() use ($req){
                $client = Client::create([
                    "name"         =>  $req->name,
                    "description"   =>  $req->description,
                    "medias_id"     =>  $req->medias_id,
                ]);
            });
            return redirect(route("admin.contents.clients"));
        } catch (Exception $e) {
            return;
        }
    }

    public function clientContentRemove($clients_id)
    {
        try {
            DB::transaction(function() use ($clients_id){
                if (Client::where("id", $clients_id)->count() > 0)
                {
                    Client::where("id", $clients_id)->forceDelete();
                }
            });
            return redirect(route("admin.contents.clients"));
        } catch (Exception $e) {
            return;
        }
    }

    public function clientContentEdit($clients_id)
    {
        return view("admin.contents.client.create")->with([
            "client"    => Client::where("id", $clients_id)->first(),
            "medias"    => Media::get()
        ]);
    }

    public function clientContentUpdate(Request $req, $clients_id)
    {
        try {
            if (Client::where("id", $clients_id)->count() > 0)
            {
                Client::where("id", $clients_id)->update([
                    "name"          =>  $req->name,
                    "description"   =>  $req->description,
                    "medias_id"     =>  $req->medias_id,
                ]);
            }
            return redirect(route("admin.contents.clients"));
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * ================================================
     */

    public function teamContent()
    {
        return view("admin.contents.team.list")->with([
            "teams" => Team::get()
        ]);
    }

    public function teamContentCreate()
    {
        return view("admin.contents.team.create")->with([
            "medias" => Media::get()
        ]);
    }

    public function teamContentStore(Request $req)
    {
        try {
            DB::transaction(function() use ($req){
                $team = Team::create([
                    "name"          =>  $req->name,
                    "medias_id"     =>  $req->medias_id,
                ]);
            });
            return redirect(route("admin.contents.teams"));
        } catch (Exception $e) {
            return;
        }
    }

    public function teamContentRemove($teams_id)
    {
        try {
            DB::transaction(function() use ($teams_id){
                if (Team::where("id", $teams_id)->count() > 0)
                {
                    Team::where("id", $teams_id)->forceDelete();
                }
            });
            return redirect(route("admin.contents.teams"));
        } catch (Exception $e) {
            return;
        }
    }

    public function teamContentEdit($teams_id)
    {
        return view("admin.contents.team.create")->with([
            "team"      => Team::where("id", $teams_id)->first(),
            "medias"    => Media::get()
        ]);
    }

    public function teamContentUpdate(Request $req, $teams_id)
    {
        try {
            if (Team::where("id", $teams_id)->count() > 0)
            {
                Team::where("id", $teams_id)->update([
                    "name"          =>  $req->name,
                    "medias_id"     =>  $req->medias_id,
                ]);
            }
            return redirect(route("admin.contents.teams"));
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * ================================================
     */

    public function testimonialContent()
    {
        return view("admin.contents.testimonial.list")->with([
            "testimonials" => Testimonial::get()
        ]);
    }

    public function testimonialContentCreate()
    {
        return view("admin.contents.testimonial.create")->with([
            "medias" => Media::get()
        ]);
    }

    public function testimonialContentStore(Request $req)
    {
        try {
            DB::transaction(function() use ($req){
                $testimonial = Testimonial::create([
                    "name"          =>  $req->name,
                    "description"   =>  $req->description,
                    "medias_id"     =>  $req->medias_id,//avatar
                    "bg_medias_id"  =>  $req->bg_medias_id,//product or etc
                ]);
            });
            return redirect(route("admin.contents.testimonials"));
        } catch (Exception $e) {
            return;
        }
    }

    public function testimonialContentRemove($testimonials_id)
    {
        try {
            DB::transaction(function() use ($testimonials_id){
                if (Testimonial::where("id", $testimonials_id)->count() > 0)
                {
                    Testimonial::where("id", $testimonials_id)->forceDelete();
                }
            });
            return redirect(route("admin.contents.testimonials"));
        } catch (Exception $e) {
            return;
        }
    }

    public function testimonialContentEdit($testimonials_id)
    {
        return view("admin.contents.testimonial.create")->with([
            "testimonial"      => Testimonial::where("id", $testimonials_id)->first(),
            "medias"    => Media::get()
        ]);
    }

    public function testimonialContentUpdate(Request $req, $testimonials_id)
    {
        try {
            if (Testimonial::where("id", $testimonials_id)->count() > 0)
            {
                Testimonial::where("id", $testimonials_id)->update([
                    "name"          =>  $req->name,
                    "description"   =>  $req->description,
                    "medias_id"     =>  $req->medias_id,//avatar
                    "bg_medias_id"  =>  $req->bg_medias_id,//product or etc
                ]);
            }
            return redirect(route("admin.contents.testimonials"));
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * ================================================
     */

    public function settingGeneral()
    {
        return view("admin.settings.general")->with([
            "section" => "general",
            "setting" => Setting::first(),
            "medias"  => Media::get(),
        ]);
    }

    public function settingCompany()
    {
        return view("admin.settings.general")->with([
            "section" => "company",
            "setting" => Setting::first()
        ]);
    }

    public function settingContact()
    {
        return view("admin.settings.general")->with([
            "section" => "contact",
            "setting" => Setting::first()
        ]);
    }

    public function settingGeneralStore(Request $req)
    {
        try {
            if (Setting::count() == 0)
            {
                Setting::create($req->all());
            } else {
                Setting::first()->update($req->all());
            }

            return redirect(route("admin.setting.".$req->section));
        } catch (Exception $e) {
            return;
        }

        return response()->json();
    }

    /**
     * ================================================
     */

    public function mediaContent()
    {
        return view("admin.contents.media.list")->with([
            "medias" => Media::get()
        ]);
    }

    public function mediaContentCreate()
    {
        return view("admin.contents.media.create");
    }

    public function mediaContentStore(Request $req)
    {
        try {

            foreach ($req->file('file') as $uploadedFile)
            {
                $name_file = "image_".(Media::count() + 1).".jpg";
                if (Media::where("title", $name_file)->count() == 0)
                {
                    DB::transaction(function() use ($req, $uploadedFile, $name_file){
                        $uploadedFile->move("media/", $name_file);
                        Media::create([
                            "title"  =>  $name_file,
                            "file"   =>  "media/".$name_file
                        ]);
                    });

                    return redirect(route("admin.contents.medias"));
                } else {

                    return redirect(route("admin.contents.medias"));
                }
            }

        } catch (Exception $e) {
            return;
        }
    }

    public function mediaContentRemove($medias_id)
    {
        try {
            if (Media::where("id", $medias_id)->count() > 0)
            {
                Media::where("id", $medias_id)->delete();
                return redirect(route("admin.contents.medias"));
            }
            return redirect(route("admin.contents.medias"));
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * ================================================
     */

    public function blogContent()
    {
        return view("admin.contents.blog.blog")->with([
            "posts" => Blog_post::get()
        ]);
    }

    public function blogContentCreate()
    {
        return view("admin.contents.blog.blog_create")->with([
            "tags"        => Tag::get(),
            "categories"  => Blog_category::get(),
            "medias"      => Media::get(),
        ]);
    }

    public function blogContentEdit($post_id)
    {
        $blog_post = Blog_post::where("id", $post_id)->first();
        $blog_medias = $blog_post->blog_gallery->implode('medias_id', ',');
        return view("admin.contents.blog.blog_edit")->with([
            "post" => $blog_post,
            "tags"        => Tag::get(),
            "blog_medias" => $blog_medias,
            "categories"  => Blog_category::get(),
            "medias"      => Media::get(),
        ]);
    }

    public function blogContentRemove($post_id)
    {
        try {
            if(Blog_post::where("id", $post_id)->count() > 0)
            {
                DB::transaction(function() use ($post_id){
                    Blog_post::where("id", $post_id)->delete();
                    Blog_tag::where("blog_posts_id", $post_id)->delete();
                    Blog_gallery::where("blog_posts_id", $post_id)->delete();
                    Blog_comment::where("blog_posts_id", $post_id)->delete();
                });

                return redirect(route("admin.contents.blogs"));
            }

            return redirect(route("admin.contents.blogs"));
        } catch (Exception $e) {
            return;
        }
    }

    public function blogContentStore(Request $req)
    {
        // return $req;
        try {
            DB::transaction(function() use ($req){

                $blog_post = Blog_post::create([
                    "users_id" => Auth::user()->id,
                    "blog_categories_id" => $req->blog_categories_id,
                    "blog_image_id" => $req->blog_image_id,
                    "slug" => $this->get_slug(strtolower($req->title)),
                    "summary" => substr($req->content, 0, 200),
                    "title" => $req->title,
                    "content" => $req->content,
                    "status" => "published"
                ]);

                foreach ($req->tags_id as $blog_tag)
                {
                    Blog_tag::create([
                        "blog_posts_id" => $blog_post->id,
                        "tags_id" => $blog_tag
                    ]);
                }

                foreach ($req->medias_id as $blog_gallery)
                {
                    Blog_gallery::create([
                        "blog_posts_id" => $blog_post->id,
                        "medias_id" => $blog_gallery
                    ]);
                }

            });

            return redirect(route("admin.contents.blogs"));
        } catch (Exception $e) {
            return;
        }
    }

    public function blogContentUpdate(Request $req, $post_id)
    {
        try {
            DB::transaction(function() use ($req, $post_id){

                Blog_post::where("id", $post_id)->update([
                    "blog_categories_id" => $req->blog_categories_id,
                    "blog_image_id" => $req->blog_image_id,
                    "title" => $req->title,
                    "content" => $req->content,
                    "status" => "published"
                ]);

                Blog_tag::where("blog_posts_id", $post_id)->delete();

                foreach ($req->tags_id as $blog_tag)
                {
                    Blog_tag::create([
                        "blog_posts_id" => $post_id,
                        "tags_id" => $blog_tag
                    ]);
                }


                Blog_gallery::where("blog_posts_id", $post_id)->delete();

                foreach ($req->medias_id as $blog_gallery)
                {
                    Blog_gallery::create([
                        "blog_posts_id" => $post_id,
                        "medias_id" => $blog_gallery
                    ]);
                }

            });

            return redirect(route("admin.contents.blogs"));
        } catch (Exception $e) {
            return;
        }
    }

    public function blogCategoriesContent()
    {
        return view("admin.contents.blog.category")->with([
            "categories" => Blog_category::get()
        ]);
    }

    public function blogCommentsContent()
    {
        return view("admin.contents.blog.comments")->with([
            "comments" => Blog_comment::get()
        ]);
    }

    public function blogCategoriesContentStore(Request $req)
    {
        try {
            if (Blog_category::where("name", $req->name)->count() == 0)
            {
                DB::transaction(function() use ($req){
                    Blog_category::create([
                        "name"  =>  $req->name,
                        "slug"  =>  hash("crc32", $req->name.md5(time()))
                    ]);
                });
                return redirect(route("admin.contents.blogs.categories"));
            }
            return redirect(route("admin.contents.blogs.categories"));
        } catch (Exception $e) {
            return;
        }
    }

    public function blogCategoriesContentUpdate(Request $req, $categories_id)
    {
        try {
            if (Blog_category::where("id", $categories_id)->count() > 0)
            {
                Blog_category::where("id", $categories_id)->update([
                    "name"  =>  $req->name,
                    "slug"  =>  hash("crc32", $req->name.md5(time()))
                ]);
                return redirect(route("admin.contents.blogs.categories"));
            }
            return redirect(route("admin.contents.blogs.categories"));
        } catch (Exception $e) {
            return;
        }
    }

    public function blogCategoriesContentRemove($categories_id)
    {
        try {
            if (Blog_category::where("id", $categories_id)->count() > 0)
            {
                Blog_category::where("id", $categories_id)->delete();
                return redirect(route("admin.contents.blogs.categories"));
            }
            return redirect(route("admin.contents.blogs.categories"));
        } catch (Exception $e) {
            return;
        }
    }

    public function messageContent()
    {
        return view("admin.contents.message.list")->with([
            "messages" => Message::get()
        ]);
    }

}
