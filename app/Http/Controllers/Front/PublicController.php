<?php

namespace App\Http\Controllers\Front;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Service;
use App\Models\Message;
use App\Models\Setting;

use App\Models\Team;
use App\Models\Client;
use App\Models\Testimonial;

use App\Models\Blog_tag;
use App\Models\Blog_post;
use App\Models\Blog_comment;
use App\Models\Blog_category;

use App\Models\Portofolio_post;
use App\Models\Portofolio_tag;

class PublicController extends Controller
{
    

	public function serviceContent($slug)
	{
		if (Service::where('slug',$slug)->count() > 0)
		{
			$service = Service::where('slug',$slug)->first();
			return view("public.service")->with([
				"page_title" => "Layanan - " . $service->title,
				"service" 	 => $service,
				"title"		 => "Layanan"
			]);
		}
		return redirect()->back();
	}

	public function portfolio()
	{
		return view("public.portfolio")->with([
			"page_title" => "Portofolio ",
			"portfolios" => Portofolio_post::get(),
			"portfolio_tags" => Portofolio_tag::select(DB::raw("distinct tags_id"))->get()
		]);
	}

	public function blog()
	{
		return view("public.blog")->with([
			"page_title" => "Blog",
			"blogs" => Blog_post::orderBy('id', 'DESC')->get(),
			"blog_categories" => Blog_category::get(),
			"blog_tags" => Blog_tag::select(DB::raw("distinct tags_id"))->get(),
		]);
	}

	public function blogDetail($slug_post)
	{
		return view("public.blog-detail")->with([
			"page_title" => Blog_post::where("slug", $slug_post)->first()->title . "",
			"blog" => Blog_post::where("slug", $slug_post)->first(),
			"blogs" => Blog_post::orderBy('id', 'DESC')->get(),
			"blog_categories" => Blog_category::get(),
			"blog_tags" => Blog_tag::get(),
		]);
	}

	public function blogCommentStore(Request $req, $slug_post)
	{
		try {

			if (Blog_post::where("slug", $slug_post)->count() > 0)
			{
				Blog_comment::create([
					"blog_posts_id" => Blog_post::where("slug", $slug_post)->first()->id,
					"status" => "approved",
					"name" => $req->name,
					"email" => $req->email,
					"comment" => $req->comment,
				]);
			}

			return redirect()->back();
		} catch (Exception $e) {
			return;
		}
	}

	public function contact()
	{
		return view("public.contact")->with([
			"page_title" => "Kontak",
			"setting" 	=> Setting::first(),
			"title"		=> "Kontak"
		]);
	}

	public function contactStore(Request $req)
	{
		Message::create([
			"name"  => $req->name,
			"email" => $req->email,
			"message" => $req->message
		]);

		return redirect(route("front.contact"));
	}

	public function aboutDescription()
	{
		return view("public.about-description")->with([
			"page_title" => "Desckripsi ",
			"setting" 	=> Setting::first(),
		]);
	}

	public function aboutVisiMisi()
	{
		return view("public.about-visi-misi")->with([
			"page_title" => "Visi & Misi ",
			"setting" 	=> Setting::first(),
		]);
	}

	public function aboutHistory()
	{
		return view("public.about-history")->with([
			"page_title" => "Sejarah ",
			"setting" 	=> Setting::first(),
		]);
	}

	public function aboutTeam()
	{
		return view("public.about-team")->with([
			"page_title" => "Team ",
			"teams" => Team::get()
		]);
	}

	public function aboutClient()
	{
		return view("public.about-client")->with([
			"page_title" => "Pelanggan ",
			"clients" => Client::get()
		]);
	}

	public function aboutTestimonial()
	{
		return view("public.about-testimonial")->with([
			"page_title" => "Testimoni ",
			"testimonials" => Testimonial::get()
		]);
	}

}
