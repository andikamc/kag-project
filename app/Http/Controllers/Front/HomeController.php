<?php

namespace App\Http\Controllers\Front;

use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Client;
use App\Models\Slider;
use App\Models\Testimonial;

use App\Models\Blog_tag;
use App\Models\Blog_post;
use App\Models\Blog_comment;
use App\Models\Blog_category;

use App\Models\Service;
use App\Models\Portofolio_post;
use App\Models\Portofolio_tag;

class HomeController extends Controller
{
    

	public function index()
	{
		return view("public.dashboard")->with([
			"page_title" => "Selamat Datang ",
			"portfolios" => Portofolio_post::get(),
			"portfolio_tags" => Portofolio_tag::select(DB::raw("distinct tags_id"))->get(),
			"services" => Service::get(),
			"blogs" => Blog_post::orderBy('id', 'DESC')->get(),
			"sliders" => Slider::get(),
			"testimonials" => Testimonial::get(),
			"clients" => Client::get(),
		]);
	}


}
