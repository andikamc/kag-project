<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimonial extends Model
{
    use SoftDeletes;

	protected $fillable = [
		'name',
		'description',
		'medias_id',
		'bg_medias_id',
	];	

	public function medias()
	{
		return $this->belongsTo('App\Models\Media', 'medias_id');
	}

	public function medias2()
	{
		return $this->belongsTo('App\Models\Media', 'bg_medias_id');
	}
}
