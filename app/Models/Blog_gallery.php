<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog_gallery extends Model
{
	use SoftDeletes;

    protected $table = "blog_galleries";

	protected $fillable = [
		'medias_id',
		'blog_posts_id',
	];

	public function blog_post()
	{
		return $this->belongsTo('App\Models\Blog_post', 'blog_posts_id');
	}

	public function media()
	{
		return $this->belongsTo('App\Models\Media', 'medias_id');
	}

}
