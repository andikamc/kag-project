<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    use SoftDeletes;

	protected $fillable = [
		'title',
		'description',
		'medias_id',
	];	

	public function medias()
	{
		return $this->belongsTo('App\Models\Media', 'medias_id');
	}
}
