<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

	protected $fillable = [
		'site_name',
		'site_keyword',
		'site_description',
		'footer_site_description',
		'company_description',
		'company_visi',
		'company_misi',
		'company_history',
		'site_image_dashboard',
		'site_logo',
		'site_favicon',
		'contact_email',
		'contact_address',
		'contact_phone',
		'contact_fax',
		'company_longitude',
		'company_latitude',
		'facebook_link',
		'twitter_link',
		'google_link',
		'instagram_link'
	];

	public function site_logo_image()
	{
		return $this->belongsTo('App\Models\Media', 'site_logo');
	}

	public function site_favicon_image()
	{
		return $this->belongsTo('App\Models\Media', 'site_favicon');
	}

	public function site_image_dashboard_image()
	{
		return $this->belongsTo('App\Models\Media', 'site_image_dashboard');
	}

}
