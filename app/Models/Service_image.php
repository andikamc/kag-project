<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service_image extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'medias_id',
		'services_id'
	];

	public function medias()
	{
		return $this->belongsTo('App\Models\Media', 'medias_id');
	}

	public function services()
	{
		return $this->belongsTo('App\Models\Service', 'services_id');
	}

}
