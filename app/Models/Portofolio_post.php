<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Portofolio_post extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'title',
		'content',
		'medias_id'
	];

	public function media()
	{
		return $this->belongsTo('App\Models\Media', 'medias_id');
	}

	public function portofolio_tag()
	{
		return $this->hasMany('App\Models\Portofolio_tag', 'portofolio_posts_id');
	}

}
