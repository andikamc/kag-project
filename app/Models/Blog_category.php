<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog_category extends Model
{
	use SoftDeletes;

    protected $table = "blog_categories";

	protected $fillable = [
		'name',
		'slug',
		'description'
	];
}
