<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes;

	protected $fillable = [
		'name',
		'position',
		'medias_id',
	];	

	public function medias()
	{
		return $this->belongsTo('App\Models\Media', 'medias_id');
	}
}
