<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog_tag extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'blog_posts_id',
		'tags_id'
	];

	public function blog_post()
	{
		return $this->belongsTo('App\Models\Blog_post', 'blog_posts_id');
	}

	public function tags()
	{
		return $this->belongsTo('App\Models\Tag', 'tags_id');
	}

}
