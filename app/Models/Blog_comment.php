<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog_comment extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'blog_posts_id',
		'status',
		'name',
		'email',
		'comment'
	];

	public function blog_post()
	{
		return $this->belongsTo('App\Models\Blog_post', 'blog_posts_id');
	}

}
