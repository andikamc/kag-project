<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Portofolio_tag extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'portofolio_posts_id',
		'tags_id'
	];

	public function portofolio_post()
	{
		return $this->belongsTo('App\Models\Portofolio_post', 'portofolio_posts_id');
	}

	public function tags()
	{
		return $this->belongsTo('App\Models\Tag', 'tags_id');
	}

}
