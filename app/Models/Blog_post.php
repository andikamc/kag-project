<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog_post extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'blog_categories_id',
		'blog_image_id',
		'users_id',
		'slug',
		'title',
		'content',
		'summary',
		'status'
	];

	public function blog_category()
	{
		return $this->belongsTo('App\Models\Blog_category', 'blog_categories_id');
	}

	public function blog_image()
	{
		return $this->belongsTo('App\Models\Media', 'blog_image_id');
	}

	public function blog_author()
	{
		return $this->belongsTo('App\User', 'users_id');
	}

	public function blog_tag()
	{
		return $this->hasMany('App\Models\Blog_tag', 'blog_posts_id');
	}

	public function blog_gallery()
	{
		return $this->hasMany('App\Models\Blog_gallery', 'blog_posts_id');
	}

	public function blog_comment()
	{
		return $this->hasMany('App\Models\Blog_comment', 'blog_posts_id');
	}

}
