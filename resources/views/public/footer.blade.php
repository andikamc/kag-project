<footer class="footer-sec"> 
  <!--container start-->
  <div class="container"> 
    <!--row start-->
    <div class="row"> 
      <!--col start-->
      <div class="col-md-5 col-sm-12">
        <div class="footer-info" style="color: white;">
          <div class="footer-logo">
            <a href="{{ url('') }}">
              <img class="footer-logo-default" src="{{ asset(App\Models\Setting::first()->site_logo_image->file) }}" alt="">
            </a>
          </div>
          {!! App\Models\Setting::first()->footer_site_description !!}
          <ul class="footer-social">
            <li><a href="{{ App\Models\Setting::first()->facebook_link }}"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
            <li><a href="{{ App\Models\Setting::first()->google_link }}"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
            <li><a href="{{ App\Models\Setting::first()->twitter_link }}"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
            <li><a href="{{ App\Models\Setting::first()->instagram_link }}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
          </ul>
        </div>
      </div>
      <!--col end--> 
      <!--col start-->
      <div class="col-md-7"> 
        <!--row start-->
        <div class="row"> 
          <!--col start-->
          <div class="col-md-3 col-sm-6">
            <div class="footer-info" style="display: none;">
              <h3 class="footer-title">Usefull Links</h3>
              <ul class="service-link">
                <li> <a href="index.html">Home</a> </li>
                <li> <a href="about.html">About Us</a> </li>
                <li> <a href="service.html">Services</a> </li>
                <li> <a href="projects-four.html">Gallery</a> </li>
                <li> <a href="blog.html">blog</a> </li>
                <li> <a href="faq.html">Faq</a> </li>
              </ul>
            </div>
          </div>
          <!--col end--> 
          <!--col start-->
          <div class="col-md-2 col-sm-6">
            <div class="footer-info" style="display: none;">
              <h3 class="footer-title">Our Services</h3>
              <ul class="service-link">
                <li> <a href="#">Design & Planing</a> </li>
                <li> <a href="#">Garden care</a> </li>
                <li> <a href="#">Snow removal</a> </li>
                <li> <a href="#">Preparing landscape</a> </li>
                <li> <a href="#">Forest planing</a> </li>
                <li> <a href="#">Fence grate</a> </li>
              </ul>
            </div>
          </div>
          <!--col end--> 
          <!--col start-->
          <div class="col-md-7 col-sm-6">
            <div class="footer-info">
              <h3 class="footer-title">Contact Us</h3>
              <ul class="footer-adress">
                <li><i class="fa fa-map-marker"></i><span>{!! App\Models\Setting::first()->contact_address !!}</span></li>
                <li><i class="fa fa-phone"></i><span>HP. {!! App\Models\Setting::first()->contact_phone !!}</span></li>
                <li><i class="fa fa-file"></i><span>Fax. {!! App\Models\Setting::first()->contact_fax !!}</span></li>
                <li><i class="fa fa-envelope-o"></i><span>E-mail: {!! App\Models\Setting::first()->contact_email !!}</span></li>
              </ul>
            </div>
          </div>
          <!--col end--> 
        </div>
        <!--row end--> 

      </div>
      <!--col end--> 

    </div>
    <!--row end-->
    <div class="copyright-content"> 
      <!--row start-->
      <div class="row"> 
        <!--col start-->
        <div class="col-md-12 col-sm-12 text-center">
          <p>Copyright 2018. All Rights Reserved.</p>
        </div>
        <!--col end--> 
      </div>
      <!--row end--> 
    </div>
  </div>
  <!--container end--> 
</footer>