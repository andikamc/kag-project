<section class="services bg-gray"> 
  <!--container start-->
  <div class="container">
    <div class="section-title">
      <h3>Layanan <span>KAG</span></h3>
      <p class="text-justify">
        Aktifitas kegiatan usaha selalu berupaya untuk menghasilkan produk yang tepat guna, tepat waktu dan tepat kualitas.
        PT. KAG sudah terbiasa bekerja dengan Perusahaan Swasta Asing, Swasta Nasional maupun Pemerintah dengan didukung oleh Tenaga Ahli yang berasal dari berbagai Disiplin Ilmu, dengan penerapan SIstem Manajemen Mutu telah berkembang dengan baik dan sudah banyak menyelesaikan proyek-proyek yang berskala besar, Swasta Asing, Swasta Nasional dan Pemerintah kepada Perusahaan ini.
      </p>
    </div>
    <!--row start-->
    <div class="row serviceList"> 
      @foreach($services as $service)
      <!--col start-->
      <div class="col-md-6 col-sm-6 col-xs-12" onclick="document.location='{{ route('front.service.content', $service->slug) }}';">
        <div class="single-item" style="height: 300px !important;">
          <div class="fig_caption">
            <div class="icon"><i class="fa {{ $service->icon }}"></i> </div>
            <div class="details">
              <h3><a href="{{ route('front.service.content', $service->slug) }}">{{ $service->title }}</a></h3>
              <p class="text-left"> 
                {!! substr(strip_tags($service->content), 0, 330) !!}
                @if(strlen(strip_tags($service->content))>330)
                <small>... <a href="{{ route('front.service.content', $service->slug) }}">[Read More]</a></small>
                @endif
              </p>
            </div>
          </div>
        </div>
      </div>
      <!--col end--> 
      @endforeach
    </div>
    <!--row end-->

    <!-- <div class="readmore text-center">
      <button class="main-btn btn-1 btn-1e">All Services</button>
    </div> -->
  </div>
  <!--container end--> 
</section>

<style type="text/css">
.single-item {
  height: 270px;
}
</style>