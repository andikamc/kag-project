<section class="about-info">
  <div class="about-img-pro" style="background-image:url('{{ asset(App\Models\Setting::first()->site_image_dashboard_image->file) }} ')"></div>
  <div class="container"> 
    <!--row start-->
    <div class="row"> 
      <!--col start-->
      <div class="col-md-6 col-sm-push-6">
        <div class="section-title">
          {!! App\Models\Setting::first()->site_description !!}
        </div>
      </div>
      <!--col end--> 
      <!--col start--> 

    </div>
    <!--col start--> 
  </div>
  <!--row end--> 
</section>