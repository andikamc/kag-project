@extends('public.app')

@section('content')
    <!--slider start-->
    @include('public.slider')
    <!--slider end-->

    <!-- quick-quote start-->
    @include('public.quick-quote')
    <!-- quick-quote end -->

    <!--about-info start-->
    @include('public.short-description')
    <!--about-info end--> 

    <!--services start-->
    @include('public.short-service')
    <!--services end--> 

    <!--counter start-->
    @php
    //include('public.counter')
    @endphp
    <!--counter end--> 

    <!--portfolio-area start-->
    @include('public.short-portfolio')
    <!--portfolio-area end--> 

    <!--whychoose-wrap start-->
    <!-- @@include('public.why-choose-us') -->
    <!--whychoose-wrap end--> 

    <!--Testimonials Start-->
    @include('public.testimonial')
    <!--Testimonials End--> 

    <!--blogWrapper start-->  
    @include('public.short-blog')
    <!--blogWrapper end--> 

    <!--brand-section start-->
    @include('public.client')
    <!--brand-sectionn end--> 
@endsection