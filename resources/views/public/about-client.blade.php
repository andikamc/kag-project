@extends('public.app')

@section('content')
<!--inner content start-->
<section class="inner-wrap typoghrapy">
  <div class="container"> 
    <div class="section-title">
      <h3>
        Pelanggan
      </h3>
    </div>
   <div class="row serviceList"> 
    @foreach($clients as $client)
    <!--col start-->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="single-item">
        <div class="fig_caption">
          <div class="icon">
            <img src="{{ asset($client->medias->file) }}">
          </div>
          <div class="details">
            <h3><a href="#">{{ $client->name }}</a></h3>
          </div>
        </div>
      </div>
    </div>
    <!--col end--> 
    @endforeach
  </div>
</div>
</section>
<!--inner content end--> 

<style type="text/css">
.typoghrapy .serviceList .single-item img {
  width: 200px;
  height: 100px;
}
</style>
@endsection