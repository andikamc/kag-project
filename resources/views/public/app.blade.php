<!DOCTYPE html>
<html lang="zxx">
<head>
  <meta charset="UTF-8">
  <title>{{ @$page_title }} - {{ config('app.name', 'Laravel') }}</title>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="description" content="SunRise construction & Builder company">
  <meta name="keywords" content="construction, html, template, responsive, corporate">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- Fav Icon -->
  <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
  <!-- Style CSS -->
  <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('css/blue.css') }}" rel="stylesheet">
  <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet">
  <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
  <link href="{{ asset('css/owl.css') }}" rel="stylesheet">
  <link href="{{ asset('css/jquery.fancybox.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style_slider.css') }}" rel="stylesheet">
  <link href="{{ asset('css/settings.css') }}" rel="stylesheet">
  <style type="text/css">
  .quick-quote {
    background: url("{{ asset('images/qoute-pattern-b.jpg') }}") repeat;
  }
</style>
@yield('additionalCss')
</head>
<body>
  <div class="page-wrapper"> 
    
    <!--preloader start-->
    <div class="preloader"></div>
    <!--preloader end--> 

    <!--main-header start-->
    @include('public.header')
    <!--main-header end--> 
    
    @yield('content')

    <!--footer-sec start-->
    @include('public.footer')
    <!--footer-secn end--> 
    
  </div>
  <!--scroll-to-top start-->
  <div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-up"></span></div>
  <!--scroll-to-top end--> 

  <!--jquery start--> 
  <script src="{{ asset('js/jquery-2.1.4.min.js') }}"></script> 
  <script src="{{ asset('js/bootstrap.min.js') }}"></script> 
  <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script> 
  <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script> 
  <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script> 
  <script src="{{ asset('js/jquery.fancybox.js?v=2.1.5') }}"></script> 
  <script src="{{ asset('js/owl.carousel.js') }}"></script> 
  <script src="{{ asset('js/jquery.themepunch.tools.min.js') }}"></script> 
  <script src="{{ asset('js/jquery.themepunch.revolution.min.js') }}"></script> 
  <script src="{{ asset('js/counter.js') }}"></script> 
  <script src="{{ asset('js/script.js') }}"></script> 
  @yield('additionalJs')
  <!--jquery end-->
</body>
</html>