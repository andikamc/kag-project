<section class="whychoose-wrap"> 
  <!--container start-->
  <div class="container"> 
    <!--row start-->
    <div class="row"> 
      <!--col start-->
      <div class="col-md-12">
        <div class="section-title">
          <h3>Our special support<br>
          <span>Why Choose Us </span></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquet, massa ac ornare feugiat, nunc dui auctor ipsum, sed posuere eros sapien id quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquet,</p>
        </div>
        <div class="whychoose-description">
          <ul class="row">
            <li class="col-md-6">Expert & Professional </li>
            <li class="col-md-6">Professional Approach</li>
            <li class="col-md-6"> High Quality Work</li>
            <li class="col-md-6">Satisfaction Guarantee</li>
            <li class="col-md-6"> Quick In Response</li>
            <li class="col-md-6">24/7 Emergency</li>
          </ul>
        </div>
      </div>
      <!--col end--> 
      <!--col start-->
      <!-- <div class="col-md-6">
        <div class="about-video-item">
          <div class="about-video-img"> <img src="{{ asset('images/welcome-1.jpg') }}" alt=""> <a href="https://www.youtube.com/watch?v=Vn_FGpZJqUs" class="video-play mfp-iframe xs-video"><i class="fa fa-play"></i></a> </div>
        </div>
      </div> -->
      <!--col end--> 
    </div>
    <!--row end--> 
  </div>
  <!--container end--> 
</section>