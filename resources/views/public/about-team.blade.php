@extends('public.app')

@section('content')
<!--inner content start-->
<div class="freelance-section"> 
  <!--container start-->
  <div class="container">
    <div class="section-title">
      <h3>
        Tim
      </h3>
    </div>
    <div class="freelance-content">
      <ul class="freelance-slider">
        @foreach($teams as $team)
        <li>
          <div class="teamWrap">
            <img src="{{ asset($team->medias->file) }}" class="img-responsive" alt="img1">
            <div class="caption">
              <h3>{{ $team->name }} </h3>
              <p>{{ $team->position }}</p>
            </div>
            <div class="link-wrap">
              <a href="#1">
                <i class="fa fa-facebook"></i>
              </a>
              <a href="#1">
                <i class="fa fa-twitter"></i>
              </a>
            </div>
          </div>
        </li>
        @endforeach
    </ul>
  </div>
</div>
<!--container end--> 
</div>
<!--freelance-section end--> 
@endsection