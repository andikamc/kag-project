<div class="modal fade bs-example-modal-md-2" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md-2" role="document">
    <div class="modal-content">
      <div class="top_links"><a href="#" data-dismiss="modal" aria-label="Close">Close (X)</a></div>
      <h2 class="modal-title">Isi Pesan</h2>
      <form class="login-form">
        <fieldset>
          <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="Nama" required data-error="Nama Anda harus diisi.">
          </div>
          <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="Nomor Telepon " required data-error="Nomor Telepon harus diisi.">
          </div>
          <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Alamat Email" required data-error="Alamat Email harus diisi.">
          </div>
          <div class="form-group">
            <textarea class="form-control" name="message" required rows="1" placeholder="Pesan Anda"></textarea>
          </div>
          <div class="form-group">
            <label>
              <!-- <input type="checkbox" data-error="Last Name is required.">
              <em>I agree with the terms and conditions</em> -->
            </label>
          </div>
          <button class="tg-theme-btn tg-theme-btn-lg" type="submit">Kirim</button>
        </fieldset>
      </form>
    </div>
  </div>
</div>