@extends('public.app')

@section('content')
<!--tesWrap start-->
  <div class="inner-wrap"> 
    <div class="section-title">
      <h3>
        Testimoni
      </h3>
    </div>
    <!--container start-->
    <div class="container">
      <!--row start-->
          <ul class="testimonials row">
            @foreach($testimonials as $testimonial)
      	    <li class="col-md-6 item">
              <div class="testiWrp">
                {!! $testimonial->description !!}
                <div class="clientInfo">
                  <div class="clientImg"><img alt="" src="{{ asset($testimonial->medias->file) }}" style="width: 79px; height: 79px;"></div>
                  <div class="name">{{ $testimonial->name }}</div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </li>
            @endforeach
          </ul>


      <!--row end--> 
    </div>
    <!--container end--> 
  </div>
  <!--tesWrap end--> 
  @endsection