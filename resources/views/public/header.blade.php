<header class="main-header"> 
  <!--header-top start-->
  <div class="header-top"> 
    <!--container start-->
    <div class="container"> 
      <!--row start-->
      <div class="row">
        <!-- header here -->
      </div>
      <!--row end--> 
    </div>
    <!--container end--> 
  </div>
  <!--header-top end--> 
  <!--header-upper start--> 

  <!--header-upper end--> 
  <!--header-lower start-->
  <div class="header-lower"> 
    <!--container start-->
    <div class="container">
      <div class="row">
        <div class="col-md-5 col-sm-12">
          <div class="logo-outer">
            <div class="logo"> <a href="{{ url('') }}"> <img class="logo-default" src="{{ asset(App\Models\Setting::first()->site_logo_image->file) }}"style="height: 45px;" alt="" title=""> </a> </div>
          </div>
        </div>
        <div class="col-md-7 col-sm-12">
          <div class="nav-outer clearfix menu-bg"> 
            <!--main-menu start-->
            <nav class="main-menu">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              </div>
              <div class="navbar-collapse collapse clearfix">
                @include('public.menu')
              </div>
              <div class="clearfix"></div>
            </nav>
            <!--main-menu end--> 

          </div>
        </div>
      </div>
    </div>
    <!--container end--> 
  </div>
  <!--header-lower end--> 
  <!--sticky-header start-->
  <div class="sticky-header"> 
    <!--container start-->
    <div class="container clearfix"> 
      <!--row start-->
      <div class="row"> 
        <!--col start-->
        <div class="col-md-5 col-sm-5">
          <div class="logo">
            <a href="{{ url('/') }}" class="img-responsive">
              <img class="logo-default" src="{{ asset(App\Models\Setting::first()->site_logo_image->file) }}" style="height: 45px;" alt="" title="">
            </a>
          </div>
        </div>
        <!--col end--> 
        <!--col start-->
        <div class="col-md-7 col-sm-7"> 
          <!--main-menu start-->
          <nav class="main-menu">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="navbar-collapse collapse clearfix">
              @include('public.menu')
            </div>
          </nav>
          <!--main-menu stendart--> 
        </div>
        <!--col end--> 
      </div>
      <!--row end--> 

    </div>
    <!--container end--> 
  </div>
  <!--sticky-header end--> 
</header>