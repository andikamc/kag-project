@extends('public.app')

@section('content')
@include('public.heading')
<section class="inner-wrap serviceSingle">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div id="main" role="main">
					<div class="slider">
						<div class="flexslider innerslider">
							<ul class="slides">
								@foreach($service->service_image as $image)
								<li data-thumb="{{ asset($image->medias->file) }}"> 
									<img src="{{ asset($image->medias->file) }}" alt=""/> 
								</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				<h3>Deskripsi</h3>
				<p> 
					{!! $service->content !!}
				</p>
				<!-- <h3>Features</h3>
				<ul class="featureLinks">
					<li>Donec elementum pharetra dapibus</li>
					<li>Nam dictum vestibulum diam</li>
					<li>Orci varius natoque penatibus et magnis</li>
					<li>Proin facilisis ante in turpis venenatis</li>
					<li>Vestibulum lectus ex, faucibus</li>
					<li>Morbi efficitur elit ac dolor porttitor</li>
					<li>Fusce rhoncus vehicula lacus vitae</li>
					<li>Sed porttitor risus vitae justo gravida</li>
				</ul> -->
			</div>
		</div>
	</div>
</section>
@endsection

@section('additionalCss')
<link rel="stylesheet" href="{{ asset('css/flexslider.css') }}" type="text/css" media="screen" />
<style type="text/css">
	.flex-control-nav li img {
		height: 210px;
	}
	.flex-viewport li img {
		height: 800px !important;
	}
</style>
@endsection
@section('additionalJs')
<script defer src="{{ asset('js/jquery.flexslider.js') }}"></script> 
<script>
	$(window).load(function(){
		$('.flexslider').flexslider({
			animation: "slide",
			controlNav: "thumbnails",
			start: function(slider){
				$('body').removeClass('loading');
			}
		});
	});
</script>
@endsection