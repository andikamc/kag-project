<div class="tp-banner-container sliderWraper">
  <div class="tp-banner">
    <ul>
      @foreach($sliders as $slider)
      <li data-slotamount="7" data-transition="pop" data-masterspeed="1000" data-saveperformance="on"> 
        <img alt="" src="{{ asset('images/dummy.png') }}" data-lazyload="{{ asset($slider->medias->file) }}">
        <div class="caption lft large-title tp-resizeme slidertext4" data-x="center" data-y="160" data-speed="600" data-start="1600">{{ $slider->title }}</div>
        <div class="caption lfb large-title tp-resizeme slidertext2" data-x="center" data-y="310" data-speed="600" data-start="2800">{!! $slider->description !!}</div>
        <!-- <div class="caption lfl large-title tp-resizeme slidertext3" data-x="center" data-y="370" data-speed="600" data-start="3500"> <a href="#" class="main-btn btn-1 btn-1e white_color" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target=".bs-example-modal-md-2">Get Started</a> <a href="contact.html" class="main-btn btn-1 btn-1e white_color">Contact Us</a></div> -->
      </li>
      @endforeach
    </ul>
  </div>
</div>