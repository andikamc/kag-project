@extends('public.app')

@section('content')
<section class="inner-wrap"> 
  <!--container start-->
  <div class="container"> 
    <!--row start-->
    <ul class="row blog-grid">

      <!--col start-->
      <li class="col-md-8 col-sm-12 ">
        <div class="blog-inter">
          <figure class="style-greens-two green"> 
            <img src="{{ asset($blog->blog_image->file) }}" style="width: 100%;" alt="portfolio">
            <!-- <div><i class="fa fa-plus"></i></div> -->
          </figure>
          <div class="post-tittle">
            <h4><a>{{ $blog->title }}</a></h4>
          </div>
          <ul class="blogDate">
            <li> <i class="fa fa-user" aria-hidden="true"></i><span>{{ $blog->blog_author->name }}</span> </li>
            <li> <i class="fa fa-calendar" aria-hidden="true"></i> <span>{{ Carbon\Carbon::parse($blog->created_at)->format('d M Y') }}</span> </li>
            <li> <i class="fa fa-comments" aria-hidden="true"></i> <span> {{ count($blog->blog_comment) }} Comments</span> </li>
          </ul>
          <div class="divider"></div>
          <p>{!! $blog->content !!}</p>
        </div>


        <div class="commentArea">
          <h3>Comments</h3>
          <div class="blog-comments">
            <div class="row">
              @if($blog->blog_comment->count() == 0)
              <div class="col-sm-2">
                No comments.
              </div>
              @endif
              @foreach($blog->blog_comment as $comment)
              <div class="col-sm-2">
                <div class="comments-img"> <img src="https://www.qualiscare.com/wp-content/uploads/2017/08/default-user-300x300.png" alt=""> </div>
              </div>
              <div class="col-sm-10">
                <div class="comment">
                  <h5> {{ $comment->name }} <span>{{ Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</h5>
                    <p>{{ $comment->comment }}</p>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
            <br>
            <hr>
            <h3>Post A Comment</h3>
            <form action="{{ route('front.blog.comment.store', $blog->slug) }}" method="post" class="formSec">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" name="name" id="name" placeholder="Name" class="form-control" style="color: black;">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" name="email" id="email" placeholder="Email" class="form-control" style="color: black;">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <textarea rows="8" name="comment" id="message" placeholder="Write comment here ..." class="form-control" style="color: black;"></textarea>
                  </div>
                </div>
              </div>
              <div class="readmore">
                @csrf
                <button class="main-btn btn-1 btn-1e" type="submit">Submit</button>
              </div>
            </form>
          </div>
        </li>
        <!--col end-->

        <li class="col-md-4 col-sm-12">
          <div class="side-bar" style="display: none;">
            <!-- Search -->
            <div class="side-barBox">
              <h5 class="side-barTitle">Search</h5>
              <div class="search">
                <form>
                  <input type="text" class="form-control" placeholder="Search">
                  <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                </form>
              </div>
            </div>
          </div>

          <div class="side-bar"> 
            <!-- Tags -->
            <div class="side-barBox">
              <h5 class="side-barTitle">Tags</h5>
              <ul class="tags-bar">
                @foreach($blog->blog_tag as $tag)
                <li><a href="{{ route('front.blog.tag', $tag->tags->slug) }}">{{ $tag->tags->name }}</a></li>
                @endforeach
              </ul>
              <div class="clearfix"></div>
            </div>
          </div>

          <div class="side-bar"> 
            <div class="side-barBox">
              <h5 class="side-barTitle">Blog Gallery</h5>
              <ul class="photo-bar">
                @if($blog->blog_gallery->count() == 0) <li>No images found.</li> @endif
                @foreach($blog->blog_gallery as $gallery)
                <li><a href="#"><img src="{{ asset($gallery->media->file) }}" alt=""></a></li>
                @endforeach
              </ul>
              <div class="clearfix"></div>
            </div>
          </div>

          <div class="side-bar"> 
            <!-- Categories -->
            <div class="side-barBox">
              <h5 class="side-barTitle">Categories</h5>
              <ul class="categories">
                @foreach($blog_categories as $category)
                <li><a href="{{ route('front.blog.category', $category->slug) }}"> {{ $category->name }}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="side-bar"> 
            <!-- Recent List -->
            <div class="side-barBox">
              <h5 class="side-barTitle">Recent List</h5>
              <ul class="papimg-post">
                @foreach($blogs as $blog)
                <li>
                  <div class="media-left"> <a href="{{ route('front.blog.detail', $blog->slug) }}"><img src="{{ asset($blog->blog_image->file) }}" alt="{{ $blog->title }}"></a> </div>
                  <div class="media-body"> <a class="media-heading" href="{{ route('front.blog.detail', $blog->slug) }}">{{ $blog->title }}</a> <span>{{ Carbon\Carbon::parse($blog->created_at)->format('M d. Y') }}</span> </div>
                </li>
                @endforeach
              </ul>
            </div>
          </div>

        </li>

      </ul>
    <!-- <div class="pagination-area">
      <ul class="pagination">
        <li class="active"><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">6</a></li>
        <li><a href="#">7</a></li>
        <li><a href="#">8</a></li>
      </ul>
    </div> -->
    <!--row end--> 

  </div>
  <!--container end--> 
</section>
@endsection