<div class="testimonials-wrap" style="background: background: url(../images/testimonial.jpg) no-repeat; padding: 70px 0 35px 0; background-size: cover;">
  <div class="container">
    <div class="section-title">
      <h3 class="white_color">Testimonials</h3>
    </div>
    <ul class="owl-carousel">
      @foreach($testimonials as $testimonial)
      <li class="item" >
        <div class="testiWrp" >
          {!! $testimonial->description !!}
          <div class="clientInfo">
            <div class="clientImg"><img alt="" src="{{ asset($testimonial->medias->file) }}" style="width: 79px; height: 79px;"></div>
            <div class="name">{{ $testimonial->name }}</div>
            <div class="clearfix"></div>
          </div>
        </div>
      </li>
      @endforeach
    </ul>
  </div>
</div>