@extends('public.app')

@section('content')
  <section class="inner-wrap"> 
    <!--container start-->
    <div class="container">
      <div class="row"> </div>
      <!--row start-->
      <ul class="blog-grid">

        <!--col start-->
        <div class="col-md-8 col-sm-12">
          @foreach($blogs as $blog)
          <li>
            <div class="blog-inter">
              <div class="row">
                <div class="col-md-5 col-sm-4">
                  <figure class="style-greens-two green"> 
                    <img src="{{ asset($blog->blog_image->file) }}" alt="portfolio" style="width: 800; height: 533 !important; max-height: 534 !important;">
                    <div><i class="fa fa-plus"></i></div>
                    <a href="{{ route('front.blog.detail', $blog->slug) }}"></a> 
                  </figure>
                </div>
                <div class="col-md-7 col-sm-8">
                  <div class="post-tittle">
                    <h4><a href="{{ route('front.blog.detail', $blog->slug) }}">{{ $blog->title }}</a></h4>
                  </div>
                  <ul class="blogDate">
                    <li> <i class="fa fa-user" aria-hidden="true"></i><span> {{ explode(' ', $blog->blog_author->name)[0] }}</span> </li>
                    <li> <i class="fa fa-calendar" aria-hidden="true"></i> <span> {{ Carbon\Carbon::parse($blog->created_at)->format('d M Y') }}</span> </li>
                    <li> <i class="fa fa-comments" aria-hidden="true"></i> <span> {{ count($blog->blog_comment) }} Comments</span> </li>
                  </ul>
                  <p>{!! $blog->summary !!}</p>

                </div>
              </div>
            </div>
          </li>
          @endforeach
      </div>

        <li class="col-md-4 col-sm-12">
          <div class="side-bar" style="display: none;"> 
            <!-- Search -->
            <div class="side-barBox">
              <h5 class="side-barTitle">Search</h5>
              <div class="search">
                <form>
                  <input type="text" class="form-control" placeholder="Search">
                  <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                </form>
              </div>
            </div>
          </div>
          <div class="side-bar"> 
            <!-- Categories -->
            <div class="side-barBox">
              <h5 class="side-barTitle">Categories</h5>
              <ul class="categories">
                @foreach($blog_categories as $category)
                <li><a href="{{ route('front.blog.category', $category->slug) }}"> {{ $category->name }}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="side-bar"> 
            <!-- Recent List -->
            <div class="side-barBox">
              <h5 class="side-barTitle">Recent List</h5>
              <ul class="papimg-post">
                @foreach($blogs as $blog)
                <li>
                  <div class="media-left"> <a href="{{ route('front.blog.detail', $blog->slug) }}"><img src="{{ asset($blog->blog_image->file) }}" alt="{{ $blog->title }}"></a> </div>
                  <div class="media-body"> <a class="media-heading" href="{{ route('front.blog.detail', $blog->slug) }}">{{ $blog->title }}</a> <span>{{ Carbon\Carbon::parse($blog->created_at)->format('M d. Y') }}</span> </div>
                </li>
                @endforeach
              </ul>
            </div>
          </div>
          
          <!-- <div class="side-bar"> 
            <div class="side-barBox">
              <h5 class="side-barTitle">Photos Gallery</h5>
              <ul class="photo-bar">
                <li><a href="#"><img src="images/blog.jpg" alt=""></a></li>
                <li><a href="#"><img src="images/blog-1.jpg" alt=""></a></li>
                <li><a href="#"><img src="images/blog-2.jpg" alt=""></a></li>
                <li><a href="#"><img src="images/blog.jpg" alt=""></a></li>
                <li><a href="#"><img src="images/blog-1.jpg" alt=""></a></li>
                <li><a href="#"><img src="images/blog-2.jpg" alt=""></a></li>
                <li><a href="#"><img src="images/blog.jpg" alt=""></a></li>
                <li><a href="#"><img src="images/blog-1.jpg" alt=""></a></li>
              </ul>
              <div class="clearfix"></div>
            </div>
          </div> -->

          <div class="side-bar"> 
            <!-- Tags -->
            <div class="side-barBox">
              <h5 class="side-barTitle">Tags</h5>
              <ul class="tags-bar">
                @foreach($blog_tags as $tag)
                <li><a href="{{ route('front.blog.tag', $tag->tags->slug) }}">{{ $tag->tags->name }}</a></li>
                @endforeach
              </ul>
              <div class="clearfix"></div>
            </div>
          </div>
        </li>
      </ul>
      <!--row end--> 

    </div>
    <!--container end--> 
  </section>
@endsection  