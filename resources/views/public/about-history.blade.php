@extends('public.app')

@section('content')
<!--inner content start-->
<section class="contactWrap"> 
  <!--container start-->
  <div class="container">
    <div class="section-title">
      <h3>Sejarah<br>
      </h3>
      <h4></h4>
      <p class="text-justify">
        {!! $setting->company_history !!}
      </p>
    </div>
  </div>
</div>
<!--container end--> 
</section>
<!--inner content end--> 
@endsection