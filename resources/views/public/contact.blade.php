@extends('public.app')

@section('content')
<!--inner content start-->
<section class="contactWrap"> 
    <!--container start-->
    <div class="container">
        <div class="section-title">
            <h3>Get in <span>touch</span></h3>
            <p>
                <!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquet, massa ac ornare feugiat, nunc dui auctor ipsum, sed posuere eros sapien id quam.  -->
            </p>
        </div>
        <!--row start-->
        <div class="row serviceList"> 
            <!--col start-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="contact-item">
                    <div class="fig_caption">
                        <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i> </div>
                        <div class="details">
                            <h3>Alamat Kantor</h3>
                            <p>{!! $setting->contact_address !!}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--col end--> 
            <!--col start-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="contact-item">
                    <div class="fig_caption">
                        <div class="icon"><i class="fa fa-envelope-o" aria-hidden="true"></i> </div>
                        <div class="details">
                            <h3>E-mail</h3>
                            <p>{!! str_replace('/', '<br>', $setting->contact_email) !!}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--col end--> 
            <!--col start-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="contact-item">
                    <div class="fig_caption">
                        <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i> </div>
                        <div class="details">
                            <h3>Hubungi</h3>
                            <p>{!! str_replace('/', '<br>', $setting->contact_phone) !!}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--col end--> 
            <!--col start-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="contact-item">
                    <div class="fig_caption">
                        <div class="icon"><i class="fa fa-clock-o" aria-hidden="true"></i> </div>
                        <div class="details">
                            <h3>Jam Layanan</h3>
                            <p> <strong>Senin - Sabtu | 08:00 sampai 19:00</strong> <br>
                            Minggu : Tutup</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--col end--> 
        </div>
        <!--row end--> 
        <div class="section-title margin_t40">
            <h3>Drop <span>your massege</span></h3>

        </div>
        <form action="{{ route('front.contact.store') }}" method="POST" id="xs-contact-form" class="xs-form">
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control" name="name" placeholder="Your name" id="xs_contact_name">
                </div>
                <div class="col-md-6">
                    <input type="email" class="form-control invaild" name="email" placeholder="Your email" id="xs_contact_email">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" class="form-control" name="Subject" placeholder="Subject" id="xs_contact_subject" >
                </div>
            </div>

            <textarea name="message" placeholder="Message" id="x_contact_massage" class="form-control message-box" cols="30" rows="10"></textarea>
            <div class="readmore text-center">
                @csrf
              <button class="main-btn btn-1 btn-1e">SEND MESSAGE</button>
          </div>

      </form>
  </div>
  <!--container end--> 

</section>
<!--inner content end--> 

<div class="xs-map-sec">
    <div class="xs-maps-wraper">
        <div class="map">
            <iframe src="https://maps.google.com/maps?lat=-6.2767039&amp;long=106.9320732,17&amp;width=100&amp;height=600&amp;hl=en&amp;q=Kreasi%20Artistika%20Globalindo&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"></iframe>
        </div>
    </div>
</div>
@endsection