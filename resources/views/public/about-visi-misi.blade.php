@extends('public.app')

@section('content')
<!--inner content start-->
<section class="contactWrap"> 
  <!--container start-->
  <div class="container">
    <div class="section-title">
      <h3>VISI & MISI<br>
      </h3>
      <h4>VISI</h4>
      <p>
        {!! $setting->company_visi !!}
      </p>
      <br>
      <h4>MISI</h4>
      <p>
        {!! $setting->company_misi !!}
      </p>
    </div>
  </div>
</div>
<!--container end--> 
</section>
<!--inner content end--> 
@endsection