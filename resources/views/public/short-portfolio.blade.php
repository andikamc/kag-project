<section class="bg-gray portfolio-area"> 
  <!--container start-->
  <div class="container-fluid">
    <div class="section-title">
      <h3>Portfolio <span>KAG</span></h3>
      <p>
        Berikut adalah beberapa produk yang telah kami hasilkan. 
      </p>
    </div>
    <!--row start-->
    <div class="row">
      <ul class="container-filter categories-filter">
        <li> <a class="categories active" data-filter="*">All Projects</a> </li>
        @foreach($portfolio_tags as $tag)
        <li><a class="categories hvr-link" data-filter=".{{ $tag->tags->slug }}">{{ ucfirst($tag->tags->name) }}</a>
        @endforeach
      </ul>
    </div>
    <!--row end-->
    <div class="portfolio-inner"> 
      <!--row start-->
      <ul class="row container-masonry  portfolio-posts grid">
        @foreach($portfolios as $post)
        <!--col start-->
        <li class="col-md-2 col-sm-6 col-xs-12 nf-item grid-sizer @foreach($post->portofolio_tag as $post_tag){{ $post_tag->tags->slug }} @endforeach">
          <div class="image-hover-effect-2">
            <img src="{{ $post->media->file }}" alt="img-{{ $post->id }}">
            <div class="caption">
              <h3>{{ $post->title }}</h3>
              <p>{!! $post->content !!}</p>
            </div>
            <div class="link-wrap">
              <!-- <a href="images/gallery-kag/port3.jpg" title="Business Financing" class="lightbox-image"><i class="fa fa-search"></i></a> -->
              <!-- <a href="projects-single.html"><i class="fa fa-link"></i></a> -->
            </div>
          </div>
        </li>
        <!--col end--> 
        @endforeach
      </ul>
      <!--row end--> 
    </div>
  </div>
  <!--container end--> 
</section>