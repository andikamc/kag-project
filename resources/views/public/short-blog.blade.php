<section class="blogWrapper"> 
  <!--container start-->
  <div class="container">
    <div class="section-title">
      <h3>Our Latest<span> Blog</span></h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquet, massa ac ornare feugiat, nunc dui auctor ipsum, sed posuere eros sapien id quam. </p>
    </div>

    <!--row start-->
    <ul class="row blog-grid">

      @foreach($blogs as $blog)
      <!--col start-->
      <li class="col-md-4 col-sm-12 ">
        <div class="blog-inter">
          <figure class="style-greens-two green">
            <img src="{{ asset($blog->blog_image->file) }}" alt="sq-sample2">
            <div><i class="fa fa-plus"></i></div>
            <a href="{{ route('front.blog.detail', $blog->slug) }}"></a>
          </figure>
          <div class="post-tittle">
            <h4><a href="{{ route('front.blog.detail', $blog->slug) }}">{{ $blog->title }}</a></h4>
          </div>
          <ul class="blogDate">
            <li> <i class="fa fa-user" aria-hidden="true"></i><span>{{ explode(' ', $blog->blog_author->name)[0] }}</span> </li>
            <li> <i class="fa fa-calendar" aria-hidden="true"></i> <span> {{ Carbon\Carbon::parse($blog->created_at)->format('d M Y') }}</span> </li>
            <li> <i class="fa fa-comments" aria-hidden="true"></i> <span> {{ count($blog->blog_comment) }} Comments</span> </li>
          </ul>
          <p>{!! $blog->content !!}</p>
        </div>
      </li>
      <!--col end--> 
      @endforeach

    </ul>
    <!--row end-->
    <!-- <div class="pagination-area">
      <ul class="pagination">
        <li class="active"><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">6</a></li>
        <li><a href="#">7</a></li>
        <li><a href="#">8</a></li>
      </ul>
    </div> -->
  </div>
  <!--container end--> 
</section>