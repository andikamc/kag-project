<div class="brand-section"> 
  <!--container start-->
  <div class="container">
    <div class="brand-content">
      <ul class="brand-slider">
        @foreach($clients as $client)
        <li>
          <figure class="logo-grey-style"> <img src="{{ asset($client->medias->file) }}" alt="{{ $client->title }}" class="img-responsive">
            <figcaption>
              <h5>{{ $client->title }}</h5>
            </figcaption>
            <!-- <a href="https://themeforest.net/" target="_blank"></a>  -->
          </figure>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
  <!--container end--> 
</div>