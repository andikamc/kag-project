@extends('public.app')

@section('content')
<!--inner content start-->
<section class="contactWrap"> 
    <!--container start-->
    <div class="container">
        {!! $setting->company_description !!}
    </div>
  </div>
  <!--container end--> 
</section>
<!--inner content end--> 
@endsection