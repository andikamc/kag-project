<div class="quick-quote">
  <div class="container"> 
    <!--Row Start-->
    <div class="row">
      <div class="col-md-9">
        <!-- <h2>Looking for a quality constructor for your next project?</h2> -->
        <h2>Anda butuh Konstruktor berkualitas untuk proyek selanjutnya?</h2>
      </div>
      <div class="col-md-3">
        <div class="quote-btn">
          <a href="{{ route('front.contact') }}">Kirim Pesan</a>
        </div>
      </div>
    </div>
    <!--Row End--> 
  </div>
</div>

<!--quote-modal start-->
@include('public.quote-modal')
<!--quote-modal end-->