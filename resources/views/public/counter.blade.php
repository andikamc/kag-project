<div class="counter"> 
  <!--container start-->
  <div class="container"> 
    <!--row start-->
    <div class="row"> 
      <!--col start-->
      <div class="col-md-3 col-sm-6 col-xs-12 counter-item">
        <div class="counterbox">
          <div class="counter-icon"><i class="fa fa-list-ul" aria-hidden="true"></i></div>
          <div class="counter_area"><span class="counter-number" data-from="1" data-to="25" data-speed="1000">25</span> <span class="counter-text">year of experience</span> </div>
        </div>
      </div>
      <!--col end--> 
      <!--col start-->
      <div class="col-md-3 col-sm-6 col-xs-12 counter-item">
        <div class="counterbox">
          <div class="counter-icon"><i class="fa fa-users" aria-hidden="true"></i></div>
          <div class="counter_area"> <span class="counter-number" data-from="1" data-to="55" data-speed="2000">55</span> <span class="counter-text">Expert</span> </div>
        </div>
      </div>
      <!--col end--> 
      <!--col start-->
      <div class="col-md-3 col-sm-6 col-xs-12 counter-item">
        <div class="counterbox">
          <div class="counter-icon"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></div>
          <div class="counter_area"> <span class="counter-number" data-from="1" data-to="1756" data-speed="3000">1756</span> <span class="counter-text">Happy Customer</span> </div>
        </div>
      </div>
      <!--col end--> 
      <!--col start-->
      <div class="col-md-3 col-sm-6 col-xs-12 counter-item">
        <div class="counterbox">
          <div class="counter-icon"><i class="fa fa-trophy" aria-hidden="true"></i></div>
          <div class="counter_area_1"> <span class="counter-number" data-from="1" data-to="88" data-speed="4000">88</span> <span class="counter-text">Professional Awards</span></div>
        </div>
      </div>
      <!--col end--> 
      <!--col start--> 
    </div>
    <!--row end--> 
  </div>
  <!--container end--> 
</div>