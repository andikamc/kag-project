<ul class="navigation clearfix">
  <li class="{{ Request::is('/') ? 'current' : '' }}">
    <a class="hvr-link" href="{{ url('/') }}">Beranda</a>
  </li>
  <li class="{{ Request::is('service*') ? 'current' : '' }} dropdown">
    <a class="hvr-link" href="#">Layanan</a>
    <ul>
      @foreach(App\Models\Service::get() as $services)
      <li><a class="hvr-link" href="{{ route('front.service.content', $services->slug) }}">{{ $services->title }}</a></li>
      @endforeach
    </ul>
  </li>
  <li class="{{ Request::is('portfolio') ? 'current' : '' }}">
    <a class="hvr-link" href="{{ url('portfolio') }}">Portofolio</a>
  </li>
  <li class="{{ Request::is('blog') ? 'current' : '' }}">
    <a class="hvr-link" href="{{ url('blog') }}">Blog</a>
  </li>
  <li class="{{ Request::is('about*') ? 'current' : '' }} dropdown">
    <a class="hvr-link" href="#">Tentang</a>
    <ul>
      <li><a class="hvr-link" href="{{ url('about/description') }}">Deskripsi</a></li>
      <li><a class="hvr-link" href="{{ url('about/visimisi') }}">Visi & Misi</a></li>
      <li><a class="hvr-link" href="{{ url('about/history') }}">Sejarah</a></li>
      <li><a class="hvr-link" href="{{ url('about/team') }}">Tim</a></li>
      <li><a class="hvr-link" href="{{ url('about/client') }}">Pelanggan</a></li>
      <li><a class="hvr-link" href="{{ url('about/testimonial') }}">Testimoni</a></li>
    </ul>
  </li>
  <li class="{{ Request::is('contact') ? 'current' : '' }}">
    <a class="hvr-link" href="{{ url('contact') }}">Kontak</a>
  </li>
</ul>