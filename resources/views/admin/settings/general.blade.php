@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css">
<link href="{{ asset('css/image-picker.css') }}" rel="stylesheet">
<style type="text/css">
.image_picker_image{
	width: 250px;
}
</style>
@endpush

@section('content_header')
<h1>
	Setting
	<!-- <small>General Setting</small> -->
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Setting</a></li>
	<li class="active">General</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					{{ ucfirst($section) }}
				</h3>
				<div class="box-tools pull-right">
					<!-- <a href="{{ route('admin.contents.portofolios.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create New</a> -->
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form action="{{ route('admin.setting.general.store') }}" method="post">
					@if ($section == "general")
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Site Name</label>
								<input type="text" class="form-control" placeholder="Enter Site Name" value="{{ @$setting->site_name }}" name="site_name">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Site Keyword</label>
								<input type="text" class="form-control" placeholder="Enter Site Keyword" value="{{ @$setting->site_keyword }}" name="site_keyword">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Image Dashboard</label>
								<div style="width: 100%; height: 500px; overflow-x: auto;">
									<select class="image-picker-dashboard masonry show-html" id="imagePickerDashboard" name="site_image_dashboard">
										@foreach($medias as $media)
										<option data-img-src='{!! asset("$media->file") !!}' value="{{ $media->id }}"></option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Logo</label>
								<div style="width: 100%; height: 500px; overflow-x: auto;">
									<select class="image-picker-logo masonry show-html" id="imagePickerLogo" name="site_logo">
										@foreach($medias as $media)
										<option data-img-src='{!! asset("$media->file") !!}' value="{{ $media->id }}"></option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Favicon</label>
								<div style="width: 100%; height: 500px; overflow-x: auto;">
									<select class="image-picker-favicon masonry show-html" id="imagePickerFavicon" name="site_favicon">
										@foreach($medias as $media)
										<option data-img-src='{!! asset("$media->file") !!}' value="{{ $media->id }}"></option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Site Description</label>
								<textarea class="form-control" name="site_description" rows="10">{!! @$setting->site_description !!}</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Footer Site Description</label>
								<textarea class="form-control" name="footer_site_description" rows="10">{!! @$setting->footer_site_description !!}</textarea>
							</div>
						</div>
					</div>
					@endif
					@if($section == "company")
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Company Description</label>
								<textarea class="form-control" name="company_description" rows="6">{!! @$setting->company_description !!}</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Company Visi</label>
								<textarea class="form-control" name="company_visi" rows="5">{!! @$setting->company_visi !!}</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Company Misi</label>
								<textarea class="form-control" name="company_misi" rows="5">{!! @$setting->company_misi !!}</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Company History</label>
								<textarea class="form-control" name="company_history" rows="6">{!! @$setting->company_history !!}</textarea>
							</div>
						</div>
					</div>
					@endif
					@if($section == "contact")
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Contact Email</label>
								<input type="text" class="form-control" name="contact_email" placeholder="Enter Contact Email" value="{{ @$setting->contact_email }}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Contact Phone</label>
								<input type="text" class="form-control" name="contact_phone" placeholder="Enter Contact Phone" value="{{ @$setting->contact_phone }}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Contact Fax</label>
								<input type="text" class="form-control" name="contact_fax" placeholder="Enter Contact Fax" value="{{ @$setting->contact_fax }}">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Contact Address</label>
								<textarea class="form-control" placeholder="Enter" rows="8" name="contact_address">{!! @$setting->contact_address !!}</textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Facebook Link</label>
								<input type="text" class="form-control" placeholder="Enter Facebook Link" value="{{ @$setting->facebook_link }}" name="facebook_link">
							</div>
							<div class="form-group">
								<label>LinkedIn Link</label>
								<input type="text" class="form-control" placeholder="Enter LinkedIn Link" value="{{ @$setting->twitter_link }}" name="twitter_link">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Google+ Link</label>
								<input type="text" class="form-control" placeholder="Enter Google+ Link" value="{{ @$setting->google_link }}" name="google_link">
							</div>
							<div class="form-group">
								<label>Instagram Link</label>
								<input type="text" class="form-control" placeholder="Enter Instagram Link" value="{{ @$setting->instagram_link }}" name="instagram_link">
							</div>
						</div>
					</div>
					@endif
					<div class="pull-right">
						@csrf
						<input type="hidden" name="section" value="{{ $section }}">
						<button type="submit" class="btn btn-success">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@push("js")
<script type="text/javascript" src="//cloud.tinymce.com/stable/tinymce.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript" src="{{ asset('js/image-picker.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/jquery.masonry.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		tinymce.init({
			selector:'textarea',
			plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools colorpicker textpattern help',
			toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
			image_advtab: true,
			importcss_append: true,
			height: 400,
			image_caption: true,
		});
		setTimeout(function() {
			$('.mce-close').click();
			$('#setting_general_logo').removeAttr("disabled");
			$('#setting_general_favicon').removeAttr("disabled");
			$('#setting_general_image_dashboard').removeAttr("disabled");
		}, 1000);
		$('#setting_general_logo').select2();
		$('#setting_general_favicon').select2();
		$('#setting_general_image_dashboard').select2();
	})

	$("#imagePickerDashboard").imagepicker({
		hide_select : true,
		show_label  : false
	})
	$("#imagePickerLogo").imagepicker({
		hide_select : true,
		show_label  : false
	})
	$("#imagePickerFavicon").imagepicker({
		hide_select : true,
		show_label  : false
	})

	var container = jQuery("select.image-picker-dashboard.masonry").next("ul.thumbnails");
	container.imagesLoaded(function(){
		container.masonry({
			itemSelector:   "li",
		});
	});

	var container = jQuery("select.image-picker-logo.masonry").next("ul.thumbnails");
	container.imagesLoaded(function(){
		container.masonry({
			itemSelector:   "li",
		});
	});

	var container = jQuery("select.image-picker-favicon.masonry").next("ul.thumbnails");
	container.imagesLoaded(function(){
		container.masonry({
			itemSelector:   "li",
		});
	});

	@if(@$setting->site_image_dashboard)
	var image = "{{$setting->site_image_dashboard}}";			
	$(".image-picker-dashboard option[value='" + image + "']").prop("selected", true);
	$("#imagePickerDashboard").data('picker').sync_picker_with_select();
	@endif

	@if(@$setting->site_logo)
	var image = "{{$setting->site_logo}}";			
	$(".image-picker-logo option[value='" + image + "']").prop("selected", true);
	$("#imagePickerLogo").data('picker').sync_picker_with_select();
	@endif

	@if(@$setting->site_favicon)
	var image = "{{$setting->site_favicon}}";			
	$(".image-picker-favicon option[value='" + image + "']").prop("selected", true);
	$("#imagePickerFavicon").data('picker').sync_picker_with_select();
	@endif
</script>
@endpush