@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
<link href="{{ asset('css/image-picker.css') }}" rel="stylesheet">
<style type="text/css">
.mce-has-close{
	display: none;
}
.image_picker_image{
	width: 250px;
}
</style>
@endpush

@section('content_header')
<h1>
	Content
	<!-- <small>General Setting</small> -->
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Content</a></li>
	<li class="active">Client</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Client
				</h3>
				<div class="box-tools pull-right">
					<!-- <a href="{{ route('admin.contents.portofolios.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create New</a> -->
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form action="{{ (@$client?route('admin.contents.clients.update',$client->id):route('admin.contents.clients.store')) }}" method="post">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Name</label>
								<input type="text" class="form-control" placeholder="Enter Name" value="{{ @$client->name }}" name="name">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Description</label>
								<textarea class="form-control" name="description" style="height: 300px;">{!! @$client->description !!}</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group col-md-12">
								<label>Media</label>
								<div style="width: 100%; height: 500px; overflow-x: auto;">
									<select class="image-picker masonry show-html" id="imagePicker" name="medias_id">
										@foreach($medias as $media)
										<option data-img-src='{!! asset("$media->file") !!}' value="{{ $media->id }}"></option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="pull-right">
						@csrf
						<button type="submit" class="btn btn-success">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@push("js")
<script type="text/javascript" src="//cloud.tinymce.com/stable/tinymce.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript" src="{{ asset('js/image-picker.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/jquery.masonry.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		tinymce.init({selector:'textarea'});
		setTimeout(function() {
			$('.mce-close').click();
		}, 1000);

	})

	$("#imagePicker").imagepicker({
		hide_select : true,
		show_label  : false
	})
	var container = jQuery("select.image-picker.masonry").next("ul.thumbnails");
	container.imagesLoaded(function(){
		container.masonry({
			itemSelector:   "li",
		});
	});

	@if(@$client)
	var image = "{{$client->medias_id}}";			
	$(".image-picker option[value='" + image + "']").prop("selected", true);
	$("#imagePicker").data('picker').sync_picker_with_select();
	@endif
</script>
@endpush