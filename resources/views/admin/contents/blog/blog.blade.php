@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
@endpush

@section('content_header')
<h1>
	Blog's Manager
	<small>Manage blog content</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Contents</a></li>
	<li class="active">Blog</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Blog list
				</h3>
				<div class="box-tools pull-right">
					<a href="{{ route('admin.contents.blogs.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create New</a>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="tagList" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th style="width: 5%">#</th>
							<th>Title</th>
							<th>Category</th>
							<th>Tags</th>
							<th style="width: 25%">Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($posts as $post)
						<tr>
							<td>{{ $post->id }}.</td>
							<td>{{ $post->title }}</td>
							<td>{{ $post->blog_category->name }}</td>
							<td>
								@foreach($post->blog_tag as $blog_tag)
								@php
								$tag_html[$post->id][] = "<a>#".$blog_tag->tags->slug."</a>";
								@endphp
								@endforeach
								{!! implode(", ", $tag_html[$post->id]) !!}
							</td>
							<td>
								<a href="{{ route('admin.contents.blogs.edit', $post->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
								<a href="{{ route('admin.contents.blogs.remove', $post->id) }}" class="btn btn-danger"><i class="fa fa-remove"></i> Remove</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop

@push("js")
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#tagList').DataTable({
			'paging'      : true,
			'lengthChange': false,
			'searching'   : false,
			'ordering'    : true,
			// 'info'        : true,
			// 'autoWidth'   : false
		})
	})
</script>
@endpush