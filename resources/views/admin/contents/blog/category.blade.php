@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
@endpush

@section('content_header')
<h1>
	Category's Manager
	<small>Manage blog category content</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Contents</a></li>
	<li><a href="#">Blog</a></li>
	<li class="active"><a href="#">Categories</a></li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Category list
				</h3>
				<div class="box-tools pull-right">
					<a href="#" data-toggle="modal" data-target="#modal-default" class="btn btn-primary"><i class="fa fa-plus"></i> Create New</a>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="tagList" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th style="width: 5%">#</th>
							<th>Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($categories as $category)
						<tr>
							<td>{{ $category->id }}</td>
							<td>{{ $category->name }}</td>
							<td style="width: 20%">
								<a href="#" data-toggle="modal" data-target="#modal-slug-{{ $category->slug }}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
								<a href="{{ route('admin.contents.blogs.categories.remove', $category->id) }}" class="btn btn-danger"><i class="fa fa-remove"></i> Remove</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Modal Default -->
<div class="modal fade" id="modal-default">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Create Category</h4>
			</div>
			<form class="form-horizontal" action="{{ route('admin.contents.blogs.categories.store') }}" method="post">
				<div class="modal-body">
					<div class="box-body">
						<div class="form-group">
							<label for="inputNameSlug" class="col-sm-2 control-label">Name</label>

							<div class="col-sm-10">
								<input type="text" required="" class="form-control" id="inputNameSlug" placeholder="Name" name="name">
							</div>
						</div>
						<!-- <div class="form-group">
							<label for="inputSlugTag" class="col-sm-2 control-label">Slug</label>

							<div class="col-sm-10">
								<input type="text" required="" class="form-control" id="inputSlugTag" placeholder="Slug" name="slug">
							</div>
						</div> -->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					@csrf
					<button type="submit" class="btn btn-primary">Create</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@foreach($categories as $category)
<!-- Modal Default -->
<div class="modal fade" id="modal-slug-{{ $category->slug }}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Edit Category</h4>
			</div>
			<form class="form-horizontal" action="{{ route('admin.contents.blogs.categories.update', $category->id) }}" method="post">
				<div class="modal-body">
					<div class="box-body">
						<div class="form-group">
							<label for="inputNameSlug" class="col-sm-2 control-label">Name</label>

							<div class="col-sm-10">
								<input type="text" required="" class="form-control" id="inputNameSlug" placeholder="Name" name="name" value="{{ $category->name }}">
							</div>
						</div>
						<!-- <div class="form-group">
							<label for="inputSlugTag" class="col-sm-2 control-label">Slug</label>

							<div class="col-sm-10">
								<input type="text" required="" class="form-control" id="inputSlugTag" placeholder="Slug" name="slug">
							</div>
						</div> -->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					@csrf
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endforeach

@stop

@push("js")
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#tagList').DataTable({
			'paging'      : true,
			'lengthChange': false,
			'searching'   : false,
			'ordering'    : true,
			// 'info'        : true,
			// 'autoWidth'   : false
		})
	})
</script>
@endpush