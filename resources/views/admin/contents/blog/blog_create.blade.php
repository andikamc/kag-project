@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css">
<link href="{{ asset('css/image-picker.css') }}" rel="stylesheet">
<style type="text/css">
.image_picker_image{
	width: 250px;
}
</style>
@endpush

@section('content_header')
<h1>
	Blog's Manager
	<small>Create blog content</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Contents</a></li>
	<li class="active">Blog</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Blog
				</h3>
				<div class="box-tools pull-right">
					<a href="#" onclick="$('#formBlogPost').submit()" class="btn btn-success"><i class="fa fa-save"></i> Publish</a>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-body">
					<form class="form-horizontal" action="{{ route('admin.contents.blogs.store') }}" id="formBlogPost" method="post">
						<div class="box-body">
							<div class="form-group">
								<label for="inpuTitle" class="col-sm-2 control-label">Title</label>

								<div class="col-sm-10">
									<input type="text" required="" class="form-control" id="inpuTitle" placeholder="Title" name="title" autocomplete="off" disabled="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputContent" class="col-sm-2 control-label">Content</label>

								<div class="col-sm-10">
									<textarea disabled="" class="form-control" id="inputContent" placeholder="Content" name="content" rows="10"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="inpuTitle" class="col-sm-2 control-label">Image</label>

								<div class="col-sm-10" style="height: 500px; overflow-x: auto;">
									<select id="blog_image" class="image-picker masonry form-control" name="blog_image_id" style="width: 100%;" disabled="">
										@foreach($medias as $media)
										<option data-img-src='{!! asset("$media->file") !!}' value="{{ $media->id }}"></option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="inpuTitle" class="col-sm-2 control-label">Category</label>

								<div class="col-sm-10">
									<select id="blog_cats" class="form-control" name="blog_categories_id" style="width: 100%;" disabled="">
										<!-- <option selected="" disabled="">-- Select Tags --</option> -->
										@foreach($categories as $category)
										<option value="{{ $category->id }}" style="color: black">{{ $category->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="inpuTitle" class="col-sm-2 control-label">Tags</label>

								<div class="col-sm-10">
									<select id="blog_tags" class="form-control" name="tags_id[]" multiple="multiple" style="width: 100%;" disabled="">
										<!-- <option selected="" disabled="">-- Select Tags --</option> -->
										@foreach($tags as $tag)
										<option value="{{ $tag->id }}">#{{ $tag->slug }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="inpuTitle" class="col-sm-2 control-label">Galleries</label>

								<div class="col-sm-10" style="height: 500px; overflow-x: auto;">
									<select id="blog_medias" class="image-picker masonry form-control" name="medias_id[]" multiple="multiple" style="width: 100%;" disabled="" data-limit="6">
										<!-- <option selected="" disabled="">-- Select Tags --</option> -->
										@foreach($medias as $media)
										<option data-img-src='{!! asset("$media->file") !!}' value="{{ $media->id }}"></option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						@csrf
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@push("js")
<script type="text/javascript" src="//cloud.tinymce.com/stable/tinymce.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript" src="{{ asset('js/image-picker.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/jquery.masonry.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		tinymce.init({
			selector:'textarea',
			plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools colorpicker textpattern help',
			toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
			image_advtab: true,
			importcss_append: true,
			height: 400,
			image_caption: true,
		});
		setTimeout(function() {
			$('.mce-close').click();
			$('.box-body').show();
			$("#inpuTitle").removeAttr("disabled");
			$("#inputContent").removeAttr("disabled");
			$('#blog_cats').removeAttr("disabled");
			$('#blog_tags').removeAttr("disabled");
			$('#blog_medias').removeAttr("disabled");
			$('#blog_image').removeAttr("disabled");
		}, 360);
		$(document).ready(function() {
			$('#blog_cats').select2();
			$('#blog_tags').select2();
			// $('#blog_medias').select2();//multi
			// $('#blog_image').select2();
		});
	})

	$(".image-picker").imagepicker({
		hide_select : true,
		show_label  : false,
		limit_reached: function(){alert('Kalo kebanyakan kurang bagus, 6 aja ya')}
	})

	var container = jQuery("select.image-picker.masonry").next("ul.thumbnails");
	container.imagesLoaded(function(){
		container.masonry({
			itemSelector:   "li",
		});
	});
</script>
@endpush

