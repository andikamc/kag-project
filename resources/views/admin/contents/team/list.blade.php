@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<style type="text/css">
.td-grid{
	width: 160px;
	height: 150px;
}
.grid{
	overflow-y: auto;
}
.grid-item{
	width: 150px;
}
</style>
@endpush

@section('content_header')
<h1>
	Team's Manager
	<small>Manage team content</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Contents</a></li>
	<li class="active">Team</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Team
				</h3>
				<div class="box-tools pull-right">
					<a href="{{ route('admin.contents.teams.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create New</a>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="tagList" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th style="width: 5%">#</th>
							<th>Name</th>
							<th>Media</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($teams as $team)
						<tr>
							<td>
								{{ $team->id }}.
							</td>
							<td>
								{{ $team->name }}
							</td>
							<td class="td-grid grid">
								<img src='{{ asset($team->medias->file) }}' class="grid-item">
							</td>
							<td style="width: 20%">
								<a href="{{ route('admin.contents.teams.edit', $team->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
								<a href="{{ route('admin.contents.teams.remove', $team->id) }}" class="btn btn-danger"><i class="fa fa-remove"></i> Remove</a>
							</td>
						</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Media</th>
							<th>Action</th>
						</tr>
					</tfoot>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>


@stop

@push("js")
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#tagList').DataTable({
			'paging'      : true,
			'lengthChange': false,
			'searching'   : false,
			'ordering'    : true,
			// 'info'        : true,
			// 'autoWidth'   : false
		})
	})
</script>
@endpush