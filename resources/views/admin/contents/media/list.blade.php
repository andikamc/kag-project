@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
@endpush

@section('content_header')
<h1>
	Media's Manager
	<small>Manage image content</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Contents</a></li>
	<li class="active">Media</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Media
				</h3>
				<div class="box-tools pull-right">
					<a href="{{ route('admin.contents.medias.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create New</a>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						@foreach($medias as $media)
						<div class="col-lg-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua" style="background: url({{ asset($media->file) }});background-size: cover;">
								<div class="inner">
									<h3><i class="fa fa-image"></i></h3>

									<!-- <p alt="{{ $media->title }}">{{ substr($media->title, 0, 29) }}...</p> -->
									<p>&nbsp;</p>
									<p>&nbsp;</p>
									<p>&nbsp;</p>
								</div>
								<div class="icon">
									<!-- <i class="fa fa-shopping-cart"></i> -->
								</div>
								<a href="{{ route('admin.contents.medias.remove', $media->id) }}" class="small-box-footer" style="background-color: black;">
									Delete <i class="fa fa-remove"></i>
								</a>
							</div>
						</div>
						@endforeach
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
@stop

@push("js")
@endpush
