@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
@endpush

@section('content_header')
<h1>
	Media's Manager
	<small>Manage image content</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Contents</a></li>
	<li class="active">Media</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Media
				</h3>
				<div class="box-tools pull-right">
					<a href="#" onclick="$('#formUploadMedia').submit()" class="btn btn-success"><i class="fa fa-upload"></i> Upload</a>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form action="{{ route('admin.contents.medias.store') }}" method="post" enctype="multipart/form-data" id="formUploadMedia">
					@csrf
					<input type="file" id="input-po" name="file[]" multiple>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@push("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/fileinput.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#input-po').fileinput({
			showUpload: false,
			autoOrientImage: false,
			maxFileSize: 10240,
			elErrorContainer: '#kartik-file-errors',
			allowedFileExtensions: ["jpg", "pdf", "png"],
			multiple: true,
		});
	});
</script>
@endpush
