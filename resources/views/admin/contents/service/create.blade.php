@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
<link href="{{ asset('css/simple-iconpicker.min.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ asset('css/image-picker.css') }}" rel="stylesheet">
<style type="text/css">
.mce-has-close{
	display: none;
}
.howl-iconpicker .geticonval {
	width: 50px;
	height: 45px;
}
.incode{
	background:#efefef;
	padding:3px;
	color:#920000;
	font-family: monospace;
}
code{
	background:#efefef;
	border: 1px solid #ccc;
	padding: 10px;
	display:block;
	line-height: 18px;
}
.image_picker_image{
	width: 250px;
}
</style>
@endpush

@section('content_header')
<h1>
	Content
	<!-- <small>General Setting</small> -->
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Content</a></li>
	<li class="active">Service</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Service
				</h3>
				<div class="box-tools pull-right">
					<!-- <a href="{{ route('admin.contents.portofolios.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create New</a> -->
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form action="{{ (@$service?route('admin.contents.services.update',$service->id):route('admin.contents.services.store')) }}" method="post">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group col-md-6">
								<label>Title</label>
								<input type="text" class="form-control" placeholder="Enter Title" value="{{ @$service->title }}" name="title">
							</div>
							<div class="form-group col-md-6">
								<label class="col-md-12">Icon</label>
								<div class="col-md-9">
									<input type="text" class="input1 input form-control" placeholder="Select Icon" value="{!! @$service->icon !!}" name="icon"/>
								</div>
								<div class="col-md-3">
									<i class="fa fa-4x {!! (@$service->icon?@$service->icon:'fa-question') !!}" id="iconPreview" style="margin-top: -13px;"></i>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Content</label>
								<textarea class="form-control" name="content" style="height: 300px;">{!! @$service->content !!}</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group col-md-12">
								<label>Gallery</label>
								<div style="width: 100%; height: 500px; overflow-x: auto;">
									<select class="image-picker masonry show-html" multiple="multiple" id="imagePicker" name="medias_id[]" data-limit="6">
										@foreach($medias as $media)
										<option data-img-src='{!! asset("$media->file") !!}' value="{{ $media->id }}">Cute Kitten 1</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="pull-right">
						@csrf
						<button type="submit" class="btn btn-success">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@push("js")
<script type="text/javascript" src="//cloud.tinymce.com/stable/tinymce.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript" src="{{ asset('js/simple-iconpicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/image-picker.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/jquery.masonry.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		tinymce.init({selector:'textarea'});
		setTimeout(function() {
			$('.mce-close').click();
		}, 1000);
		$('.input1').iconpicker(".input1");
	})

	$(".input1").on('change', function () {
		$("#iconPreview").removeAttr('class')
		$("#iconPreview").attr('class','fa fa-4x '+$(".input1").val())

	})

	$("#imagePicker").imagepicker({
		hide_select : true,
		show_label  : false,
		limit_reached: function(){alert('Kalo kebanyakan kurang bagus, 6 aja ya')}
	})
	var container = jQuery("select.image-picker.masonry").next("ul.thumbnails");
	container.imagesLoaded(function(){
		container.masonry({
			itemSelector:   "li",
		});
	});
	
	@if(isset($service))
	var images = "{{$service_image}}";
	var image = images.split(",");
	console.log(image)
	$.each(image, function(i,e){
		$(".image-picker option[value='" + e + "']").prop("selected", true);
	});
	$("#imagePicker").data('picker').sync_picker_with_select();
	@endif
</script>
@endpush