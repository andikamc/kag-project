@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
@endpush

@section('content_header')
<h1>
	Message's Manager
	<small>Manage Message content</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Contents</a></li>
	<li class="active">Message</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Message
				</h3>
				<div class="box-tools pull-right">
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="tagList" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th style="width: 5%">#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Message</th>
						</tr>
					</thead>
					<tbody>
						@foreach($messages as $message)
						<tr>
							<td>
								{{ $message->id }}.
							</td>
							<td>
								{{ $message->name }}
							</td>
							<td>
								{{ $message->email }}
							</td>
							<td>
								{{ $message->message }}
							</td>
						</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Message</th>
						</tr>
					</tfoot>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>


@stop

@push("js")
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#tagList').DataTable({
			'paging'      : true,
			'lengthChange': false,
			'searching'   : false,
			'ordering'    : true,
			// 'info'        : true,
			// 'autoWidth'   : false
		})
	})
</script>
@endpush