@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
@endpush

@section('content_header')
<h1>
	Portofolio's Manager
	<small>Manage portofolio content</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Contents</a></li>
	<li class="active">Portofolio</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Media
				</h3>
				<div class="box-tools pull-right">
					<a href="{{ route('admin.contents.portofolios.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create New</a>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="tagList" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th style="width: 5%">#</th>
							<th>Title</th>
							<th>Tags</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($portofolios as $portofolio)
						<tr>
							<td>
								{{ $portofolio->id }}.
							</td>
							<td>
								{{ $portofolio->title }}
							</td>
							<td>
								@foreach($portofolio->portofolio_tag as $tag)
								@php
								$html_tag[$portofolio->id][] = "<a>#" . $tag->tags->name . "</a>"
								@endphp
								@endforeach
								{!! implode(', ', $html_tag[$portofolio->id]) !!}
							</td>
							<td style="width: 20%">
								<a href="{{ route('admin.contents.portofolios.edit', $portofolio->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
								<a href="{{ route('admin.contents.portofolios.remove', $portofolio->id) }}" class="btn btn-danger"><i class="fa fa-remove"></i> Remove</a>
							</td>
						</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Tags</th>
							<th>Action</th>
						</tr>
					</tfoot>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>


@stop

@push("js")
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#tagList').DataTable({
			'paging'      : true,
			'lengthChange': false,
			'searching'   : false,
			'ordering'    : true,
			// 'info'        : true,
			// 'autoWidth'   : false
		})
	})
</script>
@endpush