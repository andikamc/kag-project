@extends('adminlte::page')

@section('title', 'AdminLTE')

@push("css")
<link href="{{ asset('css/image-picker.css') }}" rel="stylesheet">
<style type="text/css">
.mce-has-close{
	display: none;
}
.image_picker_image{
	width: 250px;
}
</style>
@endpush

@section('content_header')
<h1>
	Content
	<!-- <small>General Setting</small> -->
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Content</a></li>
	<li class="active">Testimonial</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Testimonial
				</h3>
				<div class="box-tools pull-right">
					<!-- <a href="{{ route('admin.contents.portofolios.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create New</a> -->
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form action="{{ (@$testimonial?route('admin.contents.testimonials.update',$testimonial->id):route('admin.contents.testimonials.store')) }}" method="post">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Client Name</label>
								<input type="text" class="form-control" placeholder="Enter Client Name" value="{{ @$testimonial->name }}" name="name">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Testimonial</label>
								<textarea class="form-control" name="description" style="height: 300px;">{!! @$testimonial->description !!}</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group col-md-12">
								<label>Avatar</label>
								<div style="width: 100%; height: 500px; overflow-x: auto;">
									<select class="image-picker masonry show-html" id="imagePicker" name="medias_id">
										@foreach($medias as $media)
										<option data-img-src='{!! asset("$media->file") !!}' value="{{ $media->id }}"></option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group col-md-12">
								<label>Product / any</label>
								<div style="width: 100%; height: 500px; overflow-x: auto;">
									<select class="image-picker2 masonry show-html" id="imagePicker2" name="bg_medias_id">
										@foreach($medias as $media)
										<option data-img-src='{!! asset("$media->file") !!}' value="{{ $media->id }}"></option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="pull-right">
						@csrf
						<button type="submit" class="btn btn-success">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@push("js")
<script type="text/javascript" src="//cloud.tinymce.com/stable/tinymce.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript" src="{{ asset('js/image-picker.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/jquery.masonry.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		tinymce.init({selector:'textarea'});
		setTimeout(function() {
			$('.mce-close').click();
		}, 1000);

	})

	$("#imagePicker").imagepicker({
		hide_select : true,
		show_label  : false
	})
	$("#imagePicker2").imagepicker({
		hide_select : true,
		show_label  : false
	})

	var container = jQuery("select.image-picker.masonry").next("ul.thumbnails");
	container.imagesLoaded(function(){
		container.masonry({
			itemSelector:   "li",
		});
	});
	var container = jQuery("select.image-picker2.masonry").next("ul.thumbnails");
	container.imagesLoaded(function(){
		container.masonry({
			itemSelector:   "li",
		});
	});

	@if(@$testimonial)
	var image = "{{$testimonial->medias_id}}";			
	$(".image-picker option[value='" + image + "']").prop("selected", true);
	$("#imagePicker").data('picker').sync_picker_with_select();

	var image2 = "{{$testimonial->bg_medias_id}}";			
	$(".image-picker2 option[value='" + image2 + "']").prop("selected", true);
	$("#imagePicker2").data('picker').sync_picker_with_select();
	@endif
</script>
@endpush